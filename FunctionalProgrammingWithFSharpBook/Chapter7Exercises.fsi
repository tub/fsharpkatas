﻿module Chapter7Exercises

open Chapter7

module Vector =
  [<Sealed>]
  type Vector =
    static member (~-) : Vector -> Vector            // vector sign change
    static member (+)  : Vector * Vector -> Vector  // vector sum
    static member (-)  : Vector * Vector -> Vector  // vector difference
    static member (*)  : float * Vector -> Vector   // product with number
    static member (*)  : Vector * Vector -> float   // dot product

  val make    : float * float -> Vector     // make vector
  val coord   : Vector -> float * float     // get coordinates
  val norm    : Vector -> float             // length of vector

module Complex =
  [<Sealed>]
  type Complex =
    static member (+)  : Complex * Complex -> Complex
    static member (~-) : Complex -> Complex
    static member (-)  : Complex * Complex -> Complex
    static member (*)  : Complex * Complex -> Complex
    static member (/)  : Complex * Complex -> Complex

  val make    : float * float -> Complex
  val toF     : Complex -> float * float

module WeeklyAscending =
  [<Sealed>]
  type WeeklyAscending =
    member Item : int -> int with get

  val make      : int list -> WeeklyAscending
  val count     : WeeklyAscending * int -> int
  val insert    : WeeklyAscending * int -> WeeklyAscending
  val intersect : WeeklyAscending * WeeklyAscending -> WeeklyAscending
  val plus      : WeeklyAscending * WeeklyAscending -> WeeklyAscending
  val minus     : WeeklyAscending * WeeklyAscending -> WeeklyAscending
  val toList    : WeeklyAscending -> int list

module Poly =
  [<Sealed>]
  type Poly =
    static member (*)  : int * Poly -> Poly
    static member (*)  : Poly * Poly -> Poly
    static member (!*) : Poly -> Poly
    static member (+)  : Poly * Poly -> Poly
    member Item : int -> int with get

  val make    : int list -> Poly
  val coef    : Poly -> int list

val drawSierpinski : Unit -> Unit
val drawPeano : Unit -> Unit

module Point =
  [<Sealed>]
  type Point =
   member x : float with get
   member y : float with get

  val make : float * float -> Point

module Segment =
  [<Sealed>]
  type Segment

  val make : Point.Point * Point.Point -> Segment

module Picture =
  [<Sealed>]
  type Picture =
   member h : float
   member w : float

  val grid : float -> float -> Segment.Segment list -> Picture
  val toPointSegments : Picture -> (Point.Point * Point.Point) list
  val scale : float -> Picture -> Picture
  val scaleAndOffset : float -> float * float -> Picture -> Picture
  val beside : Picture -> Picture -> Picture
  val above : Picture -> Picture -> Picture

val man : Picture.Picture
val couple : Picture.Picture
val crowd : Picture.Picture