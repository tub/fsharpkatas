﻿module Chapter2

//--------------
// Chapter 2
//--------------

// Chapter 2 - Exercises

//2.1
let f21(n) =
  if (n % 2 = 0 || n % 3 = 0) && not (n % 5 = 0)
  then true
  else false

let ex_f21_24 = f21(24)
let ex_f21_27 = f21(27)
let ex_f21_29 = f21(29)
let ex_f21_30 = f21(30)

//2.2
let rec pow22 = function
  | (s, 0) -> ""
  | (s, n) -> s + pow22(s, n - 1)

let ex_pow22 = pow22("Boom!", 5)

//2.3
let isIthChar23(s:string, i, c) =
  if i < s.Length && i >= 0
  then s.[i] = c
  else false

let ex_isIthChar23_1 = isIthChar23("Boom!", 2, 'o')
let ex_isIthChar23_2 = isIthChar23("Boom!", 2, '!')
let ex_isIthChar23_3 = isIthChar23("Boom!", 8, 'o')

//2.4
let rec occFromIth24(s:string, i, c) =
  if i >= s.Length || i < 0
  then 0
  else if isIthChar23(s, i, c)
    then 1 + occFromIth24(s, i + 1, c)
    else occFromIth24(s, i + 1, c)

let ex_occFromIth24_1 = occFromIth24("Boom!", 1, 'o')

//2.4 - Using array methods instead instead of recursion
let rec occFromIth24a(s:string, startIndex, c) =
  s.ToCharArray()
    |> Array.indexed
    |> Array.filter (fun (i, ch) -> ch = c && i >= startIndex)
    |> Array.length

let ex_occFromIth24a_1 = occFromIth24a("Boom!", 1, 'o')

//2.5
let rec occInString25(s, c) = occFromIth24(s, 0, c)

let ex_occInString25_1 = occInString25("Boom!", 'o')

//2.6
let rec notDivisible26(d, n) = n % d <> 0

let ex_notDivisible26_1 = notDivisible26(2, 5)
let ex_notDivisible26_2 = notDivisible26(3, 9)

//2.7.1
let rec test271(a, b, c) =
  if (a <= b)
  then notDivisible26(a, c) && test271(a + 1, b, c)
  else true

let ex_test271_1 = test271(2, 3, 15)
let ex_test271_2 = test271(2, 9, 31)

//2.7.2
let rec prime272(n) = test271(2, int (sqrt(float n)), n)

let ex_prime272_1 = prime272(6)
let ex_prime272_2 = prime272(7)
let ex_prime272_3 = prime272(13)
let ex_prime272_4 = prime272(15)
let ex_prime272_5 = prime272(23)

//2.7.3
let rec nextPrime273(n) =
  if prime272(n + 1)
  then n + 1
  else nextPrime273(n + 1)

let ex_nextPrime273_1 = nextPrime273(5)
let ex_nextPrime273_2 = nextPrime273(24)

//2.8
let rec bin28 = function
  | (n, 0) -> 1
  | (n, k) when n = k -> 1
  | (n, k) when k > n -> raise (new System.InvalidOperationException())
  | (n, k) -> bin28(n-1, k-1) + bin28(n-1, k)

let ex_bin28_1 = bin28(2, 1)
let ex_bin28_2 = bin28(3, 1)
let ex_bin28_3 = bin28(4, 2)
let ex_bin28_4 = bin28(4, 3)
let ex_bin28_5 = bin28(5, 3)

//2.9
let rec f29 = function
  | (0, y) -> y
  | (x, y) -> f29(x-1, x*y)

// 1) Type = (int * int) -> int
// 2) x >= 0
// 3) f(2,3) -> f(1, 6) -> f(0, 6) -> 6
// 4) 1 * ...  * (x-2 * (x-1 * (x * y)))
//  = 1 * 2 * ... * x-2 * x-1 * x * y
//  = x! * y

//2.10
let test210(c, e) = if c then e else 0

// 1) (bool * int) -> int
// 2) stack overflow
// 3) 0

//2.11
let VAT211 n x = n * x
let unVAT211 n x = x / (float n)

let ex_unVat211_1 = unVAT211 0.10 (VAT211 0.10 50.0)

//2.12 - using an inner function
let min212 f =
  let rec min_inn f n =
    if f(n) = 0
    then n
    else min_inn f n + 1
  min_inn f 1

//2.13
let curry213 f = (fun x -> (fun y -> f(x,y)))
let uncurry213 f = (fun (x,y) -> f(x)(y))

//use test210 to test currying
let ex_curry213_1 = curry213(test210)(true)(5)

//and use the VAT function to test uncurrying
let ex_uncurry213_1 = uncurry213(VAT211)(0.10, 50.0)

//of course we should be able to apply one to the other
let ex_uncurry213_2 = uncurry213(curry213(test210))(true, 5)
let ex_curry213_2 = curry213(uncurry213(VAT211))(0.10)(50.0)


