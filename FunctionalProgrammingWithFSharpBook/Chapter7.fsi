﻿namespace Chapter7

module Vector =
  [<Sealed>]
  type Vector =
    static member (~-) : Vector -> Vector            // vector sign change
    static member (+)  : Vector * Vector -> Vector  // vector sum
    static member (-)  : Vector * Vector -> Vector  // vector difference
    static member (*)  : float * Vector -> Vector   // product with number
    static member (*)  : Vector * Vector -> float   // dot product

  val make    : float * float -> Vector     // make vector
  val coord   : Vector -> float * float     // get coordinates
  val norm    : Vector -> float             // length of vector

module Queue =
  [<Sealed>]
  type Queue<'a when 'a : comparison> =
    interface System.IComparable
    member Item : int -> 'a with get
  val empty : Queue<'a>
  val put   : 'a -> Queue<'a> -> Queue<'a>
  val get   : Queue<'a> -> 'a * Queue<'a>
  exception EmptyQueue

module Curve =
  [<Sealed>]
  type Curve =
    static member (+) : Curve * Curve -> Curve
    static member (*) : float * Curve -> Curve
    static member (|^) : Curve * float -> Curve
    static member (|^) : Curve * int -> Curve
    static member (-->) : Curve * (float * float) -> Curve
    static member (><) : Curve * float -> Curve
  val point : float * float -> Curve
  val verticRefl : Curve -> float -> Curve
  val boundingBox : Curve -> (float * float) * (float * float)
  val width : Curve -> float
  val height : Curve -> float
  val toList : Curve -> (float * float) list