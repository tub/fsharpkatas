﻿module Chapter9

open System.Collections.Generic

// Ex 9.2 - show the gcd function is iterative

let rec gcd = function
  | (0, n) -> n
  | (m, n) -> gcd(n % m, m)

// definition of iterative is 
// let rec g z = if p z then g(f z) else hz;;

// gcd can be rewritten to match the form as follows

let rec gcdA (m, n) =
  let f (m, n) = (n % m, m)
  if m <> 0 then gcdA (f (m, n)) else n

let ex_gcdA = gcdA (12, 27) // -> 3

// Ex 9.3 - declare an iterative version of Ex 1.6

// 1.6 - m + (m + 1) + (m + 2) + .... (m + n - 1) + (m + n)
// simplified =  m*n + (1 + 2 + ... (n - 1) + n)
let rec sum16 = function
  | (m, 0) -> 0
  | (m, n) -> m + n + sum16(m, n - 1)

let rec sumA (m, n, a) =
  if n <> 0 then sumA (m, n-1, a + m + n) else a

let ex_sum = sumA(5, 6, 0) // -> 51

// Ex 9.4 - give an iterative version of the function List.length

let rec lenA (l, a) =
  if l <> [] then lenA(List.tail l, a + 1) else a

let ex_lenA = lenA ([4; 2; 8], 0)

// Ex 9.5 Express the function List.fold in terms of an iterative function
// itfold, iterating a function of type  'a list * 'b -> 'a list * b

// Book 9.5 fold definition

let rec fold_95 f e xs =
  if not (List.isEmpty xs)
  then fold_95 f (f e (List.head xs)) (List.tail xs)
  else e

// I "think" what they want us to do is make a function that matches
// the (f) function in the definition of an iterative function below.
// It's hard to be sure though.
// let rec g z = if p z then g(f z) else hz;;
let rec itfold f z =
  let itf (e, xs) = ((f e (List.head xs)), (List.tail xs))
  let p (e, xs) = not (List.isEmpty xs)
  if (p z)
  then itfold f (itf z)
  else z

// simple reverse list fold example
let ex_itfold = itfold (fun acc x -> [x] @ acc) ([], [1; 2; 3; 4;])

// Ex 9.6 Declare a continuation-based version of the factorial function and
// compare the run time with the results in Section 9.4

let rec factN_93 = function // Naive factorial function
  | 0 -> 1
  | n -> n * factN_93(n - 1)

let rec factA_94 = function // Section 9.4 iterative factorial function
  | (0, m) -> m
  | (n, m) -> factA_94(n - 1, n * m)

let rec factC n c =
  if n = 0
  then c()
  else factC (n - 1) (fun () -> n * c() )

// #time
// let xs16 = List.init 1000000 (fun i -> 16)

// for i in xs16 do let _ = factN_93 i in ()
// Real: 00:00:00.052, CPU: 00:00:00.062, GC gen0: 0, gen1: 0, gen2: 0

// for i in xs16 do let _ = factA_94 (i, 1) in ()
// Real: 00:00:00.028, CPU: 00:00:00.031, GC gen0: 0, gen1: 0, gen2: 0

// for i in xs16 do let _ = (factC i (fun () -> 1)) in ()
// Real: 00:00:00.322, CPU: 00:00:00.312, GC gen0: 170, gen1: 2, gen2: 1

// Ex 9.7 - Fibonacci versions

// from Ex 1.5 (converted to use longs)
let rec fibonacci = function
  | 0L -> 0L
  | 1L -> 1L
  | n -> fibonacci(n - 1L) + fibonacci(n - 2L)

// #time
// > fibonacci 40L;;
// Real: 00:00:01.155, CPU: 00:00:01.156, GC gen0: 0, gen1: 0, gen2: 0
// val it : int64 = 102334155L

//> fibonacci 50L;;
//Real: 00:02:21.599, CPU: 00:02:21.203, GC gen0: 0, gen1: 0, gen2: 0
//val it : int64 = 12586269025L

// Iterative version using two parameters to accumulate.
// n1 should be 1, n2 should be 0 to initialize
let rec fibA n n1 (n2:int64) =
  if n <> 0L
  then fibA (n - 1L) (n1 + n2) n1
  else n2

let ex_fibA = fibA 6L 1L 0L // -> 8

// #time
// > fibA 1000L 1L 0L;;
// Real: 00:00:00.000, CPU: 00:00:00.000, GC gen0: 0, gen1: 0, gen2: 0
// val it : int64 = 817770325994397771L


// Continuation version, matching the initial function
let rec fibC n (c:int64 -> int64) =
  if n <= 1L
  then c n
  else fibC (n-1L) (fun x -> fibC (n-2L) (fun y -> c (x + y)))

// > fibC 40L id;;
// Real: 00:00:12.395, CPU: 00:00:12.406, GC gen0: 4211, gen1: 2, gen2: 0
// val it : int64 = 102334155L

// > fibC 45L id;;
// Real: 00:02:24.049, CPU: 00:02:23.875, GC gen0: 46700, gen1: 14, gen2: 1
// val it : int64 = 1134903170L

// let ex_fibC = fibC 5L id
// breakdown:
// = fibC 4L (fun x -> (fibC 3L (fun y -> id (x + y))))
// = fibC 3L (fun x -> (fibC 2L (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (x + y)))))
// = fibC 2L (fun x -> (fibC 1L (fun y -> ((fun x -> (fibC 2L (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (x + y))))) (x + y)))))
// = fibC 1L (fun x -> (fibC 0L (fun y -> ((fun x -> (fibC 1L (fun y -> ((fun x -> (fibC 2L (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (x + y))))) (x + y))))) (x + y)))))
// = fibC 0L (fun y -> ((fun x -> (fibC 1L (fun y -> ((fun x -> (fibC 2L (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (x + y))))) (x + y))))) (1L + y)))
// = (fun x -> (fibC 1L (fun y -> ((fun x -> (fibC 2L (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (x + y))))) (x + y))))) (1L + 0L)
// = fibC 1L (fun y -> ((fun x -> (fibC 2L (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (x + y))))) (1L + y))) 
// = (fun x -> (fibC 2L (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (x + y))))) (1L + 1L) 
// = fibC 2L (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (2L + y)))
// = fibC 1L (fun x -> (fibC 0L (fun y -> (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (2L + y))) (x + y))))
// = fibC 0L (fun y -> ((fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (2L + y))) (1L + y)))
// = (fun y -> ((fun x -> (fibC 3L (fun y -> id (x + y)))) (2L + y))) (1L + 0L)
// = (fun x -> (fibC 3L (fun y -> id (x + y)))) (2L + 1L)
// = fibC 3L (fun y -> id (3L + y))
// = fibC 2L (fun x -> (fibC 1L ((fun y -> (fun y -> id (3L + y)) (x + y)))))
// = fibC 1L (fun x -> (fibC 0L (fun y -> ((fun x -> (fibC 1L ((fun y -> (fun y -> id (3L + y)) (x + y))))) (x + y)))))
// = fibC 0L (fun y -> ((fun x -> (fibC 1L ((fun y -> (fun y -> id (3L + y)) (x + y))))) (1L + y)))
// = (fun x -> (fibC 1L ((fun y -> (fun y -> id (3L + y)) (x + y))))) (1L + 0L)
// = fibC 1L ((fun y -> (fun y -> id (3L + y)) (1L + y)))
// = (fun y -> id (3L + y)) (1L + 1L)
// = id (3 + 2)
// = 5

// This is wierd and works, but doesn't actually use tail recursion
// It's no better than the original fibonacci
let rec fibC2 n (c:int64 -> int64) =
  if n <= 1L
  then c n
  else fibC2 1L (fun _ -> (fibC2 (n-1L) c) + (fibC2 (n-2L) c))

// > fibC2 40L id;;
// Real: 00:00:09.711, CPU: 00:00:09.718, GC gen0: 2105, gen1: 1, gen2: 0
// val it : int64 = 102334155L


// This works but I don't know how!?
// It would probably require some nasty maths to figure that out.
// It's not actually tail recursive anyway. it will end up blowing the stack
let rec fibC3 n (c:int64 -> int64) =
  if n <= 1L
  then c n
  else fibC3 (n-1L) (fun x -> x + (fibC3 (n-2L) c))

// > fibC3 25L id;;
// Process is terminated due to StackOverflowException.


// - So fibA is by far the fastest, while using little memory
// - fibonacci doesn't seem to use that much memory. I guess it's more
// CPU limited than stack space limited
// - fibC runs crazy slowly, using up lots of CPU. Don't bother with it


// Ex 9.8 - Develop a version of the counting function for
//          binary trees with the type: 
//          countA : int -> BinTree<'a> -> int

type BinTree<'a> = 
  | Leaf
  | Node of BinTree<'a> * 'a * BinTree<'a>

// from section 9.6 of the book
let rec genTree xs =
  match xs with
    | [| |] -> Leaf
    | [| x |] -> Node(Leaf, x, Leaf)
    | _ -> let m = xs.Length / 2
           let xsl = xs.[0..m-1]
           let xm = xs.[m]
           let xsr = xs.[m+1 ..]
           Node( genTree xsl, xm, genTree xsr)

// from section 9.6 of the book
let rec count = function
  | Leaf -> 0
  | Node(tl, n, tr) -> count tl + count tr + 1

// from section 9.6 of the book
let rec countC t c =
  match t with
  | Leaf -> c 0
  | Node(tl, n, tr) ->
    countC tl (fun vl -> countC tr (fun vr -> c(vl + vr + 1))) 

let rec countA a t =
  match t with
  | Leaf -> a
  | Node(tl, n, tr) -> countA (a+1) tl + countA 0 tr

// Ex 9.9 - Previous isn't tail recursive, make a tail recursive
//          version using contiuations with the type:
//          countAC : BinTree<'a> -> int -> (int -> 'b) -> 'b
//          such that count t = countAC t 0 id

let rec countAC t a c =
  match t with
  | Leaf -> c (a)
  | Node(tl, n, tr) -> countAC tl (a + 1) (fun x -> countAC tr 0 (fun y -> c (x + y)))

// t
// tl | tr
// tll tlr | trl tlr

// = countAC t 0 id
// = countAC tl 1 (fun x -> countAC tr 0 (fun y -> id (x + y)))
// = countAC tll 2 (fun x -> countAC tlr 0 (fun y -> ((fun x -> countAC tr 0 (fun y -> id (x + y))) (x + y))))
// = countAC tlr 0 (fun y -> ((fun x -> countAC tr 0 (fun y -> id (x + y))) (2 + y)))
// = (fun x -> countAC tr 0 (fun y -> id (x + y))) (2 + 0)
// = countAC tr 0 (fun y -> id (2 + y))
// = countAC trl 1 (fun x -> countAC trr 0 (fun y -> ((fun y -> id (2 + y)) (x + y))))
// = countAC trr 0 (fun y -> ((fun y -> id (2 + y)) (1 + y)))
// = (fun y -> id (2 + y)) (1 + 0)
// = id (2 + 1))
// = 3


// Ex 9.10 - Why does the following overflow?

let rec bigListK n k =
  if n = 0
  then k []
  else bigListK (n - 1) (fun res -> 1::k(res))

// because the labda function on the else line:
//   (fun res -> 1::k(res)
// is not tail recursive. it has to wait for k to return before it can
// append the 1

// Ex 9.11 - Declare tail recursive functions leftTree and rightTree
//           They should create unblanaced trees so that one side is
//           fillled from n down to 0, with the other side being empty.
//           Then:
//           (1) use them to show the stack limit using count and countA
//           (2) measure the perf of countC and countAC

let rec leftTree a n =
  if n = 0
  then a
  else 
    match a with
    | Leaf -> leftTree (Node (a, 0, Leaf)) (n - 1)
    | Node(_, x, _) -> leftTree (Node (a, x + 1, Leaf)) (n - 1)

let rec rightTree a n =
  if n = 0
  then a
  else 
    match a with
    | Leaf -> rightTree (Node (Leaf, 0, a)) (n - 1)
    | Node(_, x, _) -> rightTree (Node (Leaf, x + 1, a)) (n - 1)

// (1)
// > count (rightTree Leaf 60000);;
// val it : int = 60001
// > count (rightTree Leaf 70000);;
// Process is terminated due to StackOverflowException.
// (Same as above for leftTree)

// > countA 0 (rightTree Leaf 50000);;
// val it : int = 50001
// > countA 0 (rightTree Leaf 60000);;
// Process is terminated due to StackOverflowException.
// (Same as above for leftTree)

// (2)
// > countAC (leftTree Leaf 10000000) 0 id;;
// Real: 00:00:03.265, CPU: 00:00:03.265, GC gen0: 164, gen1: 43, gen2: 2
// val it : int = 10000001
// > countAC (rightTree Leaf 10000000) 0 id;;
// Real: 00:00:04.334, CPU: 00:00:04.734, GC gen0: 89, gen1: 73, gen2: 2
// val it : int = 10000001
// > countAC (rightTree Leaf 79000000) 0 id;;
// Real: 00:01:32.842, CPU: 00:01:30.546, GC gen0: 703, gen1: 451, gen2: 14
// val it : int = 79000001
// > countAC (rightTree Leaf 80000000) 0 id;;
// > System.OutOfMemoryException: Exception of type 'System.OutOfMemoryException' was thrown.
// Stopped due to error

// > countC (leftTree Leaf 10000000) id;;
// Real: 00:00:03.502, CPU: 00:00:03.515, GC gen0: 165, gen1: 42, gen2: 1
// val it : int = 10000001
// > countC (rightTree Leaf 10000000) id;;
// Real: 00:00:04.350, CPU: 00:00:04.765, GC gen0: 90, gen1: 65, gen2: 2
// val it : int = 10000001
// > countC (rightTree Leaf 79000000) id;;
// Real: 00:01:24.842, CPU: 00:01:24.375, GC gen0: 700, gen1: 450, gen2: 13
// val it : int = 79000001
// > countC (rightTree Leaf 80000000) id;;
// > System.OutOfMemoryException: Exception of type 'System.OutOfMemoryException' was thrown.
// Stopped due to error

// Ex 9.12 - Develop a contiunuation version of preOrder and compare perf

// from section 6.4 of the book
let rec preOrder = function
  | Leaf -> []
  | Node(tl, x, tr) -> x :: (preOrder tl) @ (preOrder tr)

let rec preOrderC n c =
  match n with
  | Leaf -> c([])
  | Node(tl, x, tr) -> preOrderC tl (fun l -> preOrderC tr (fun r ->  c(x :: l @ r) ))

// preOrderC (genTree [|1..100|] id


// Ex 9.13 - Comare run times of the two different versions of tryFind in the book

// tryFind takes a predicate p, and a set s
// we find the minElement of the set s
// then check if the predicate matches that minElement
// If it does match return the min element,
// otherwise remove the min element and try again

// from section 5.2 of the book
let rec tryFind p s =
  if Set.isEmpty s then None
  else
    let minE = Set.minElement s
    if p minE then Some minE
    else tryFind p (Set.remove minE s)

// from section 8.12
let enumerator (m: IEnumerable<'c>) =
  let e = ref (m.GetEnumerator())
  let f () =
    match (!e).MoveNext() with
    | false -> None
    | _ -> Some ((!e).Current)
  f
let tryFind2 p (s: Set<'a>) =
  let f = enumerator s
  let rec tFnd () = 
    match f() with
    | None -> None
    | Some x ->
      if (p x)
      then Some x
      else tFnd()
  tFnd()

//e.g: predicate matches a set of size 4, set passed in is a set of sets
//let ss = set [set [1;3;5]; set [2;4]; set [7;8;9] ]
//ignore (tryFind (fun s -> Set.count s = 3) ss)

let rec genListOfLists (a: 'a list list) (l: 'a list) =
  match a, l with
  | _, []  -> a
  | [], x::xs -> genListOfLists ([x]::[]) xs
  | y::ys, x::xs -> genListOfLists ((x::y)::a) xs

// e.g. genListOfLists [] [1; 2; 3]
// > [[3; 2; 1]; [2; 1]; [1]]

let rec genSetofSets n =
  genListOfLists [] [1..n]
    |> Set.ofList 
    |> Set.map Set.ofList 


// > #time;;
// > let input = (genSetofSets 1000);;
//
// > tryFind (fun s -> Set.count s = 1000) input;;
// Real: 00:00:00.776, CPU: 00:00:00.703, GC gen0: 169, gen1: 1, gen2: 1
// val it : Set<int> option = Some (set [1; 2; 3; 4; 5; 6; 7; 8; 9; ...])
//
// > tryFind2 (fun s -> Set.count s = 1000) input;;
// Real: 00:00:00.009, CPU: 00:00:00.000, GC gen0: 0, gen1: 0, gen2: 0
// val it : Set<int> option = Some (set [1; 2; 3; 4; 5; 6; 7; 8; 9; ...])

// > #time;;
// > let input = (genSetofSets 3000);;
//
// > tryFind (fun s -> Set.count s = 3000) input;;
// Real: 00:00:03.478, CPU: 00:00:03.453, GC gen0: 1749, gen1: 0, gen2: 0
// val it : Set<int> option = Some (set [1; 2; 3; 4; 5; 6; 7; 8; 9; ...]) 
// 
//
// > tryFind2 (fun s -> Set.count s = 3000) input;;
// Real: 00:00:00.045, CPU: 00:00:00.046, GC gen0: 0, gen1: 0, gen2: 0
// val it : Set<int> option = Some (set [1; 2; 3; 4; 5; 6; 7; 8; 9; ...])

// tryFind2 is much much much faster