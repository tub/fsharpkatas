﻿module Chapter3

//--------------
// Chapter 3
//--------------

let solve(a, b, c) =
  let sqrtD =
    let d = b*b - 4.0*a*c
    if d < 0.0 || a = 0.0
    then failwith "discriminant is negative or a=0.0"
    else sqrt d
  ((-b + sqrtD) / (2.0*a), (-b - sqrtD) / (2.0*a))

type Shape =
  | Circle of float
  | Square of float
  | Triangle of float*float*float

let isShape = function
  | Circle r -> r > 0.0
  | Square a -> a > 0.0
  | Triangle(a, b, c) ->
    a > 0.0 && b > 0.0 && c > 0.0
    && a < b + c && b < c + a && c < a + b

let area x =
  if not (isShape x)
  then failwith "not a legal shape"
  else
    match x with
    | Circle r -> System.Math.PI * r * r
    | Square a -> a * a
    | Triangle(a, b, c) ->
      let s = (a + b + c)/2.0
      sqrt(s*(s - a)*(s - b)*(s - c))

// Exercises

//3.1
let before31 = function
  | ((h1, m1, t1), (h2, m2,  t2)) -> (t1, h1, m1) < (t2, h2, m2)

let (&<) t1 t2 = before31(t1, t2)

let ex_before31_1 = (9, 40, "AM") &< (11, 1, "AM")
let ex_before31_2 = (9, 40, "AM") &< (2, 2, "PM")
let ex_before31_3 = (4, 40, "PM") &< (5, 50, "AM")

type ampm =
  | AM
  | PM

type time31 = {period: ampm; hour: int; min: int;}

let before31a(t1, t2) = t1 < t2

let ex_before31a_1 = before31a({hour = 9; min = 40; period = AM}, {hour = 11; min = 1; period = AM})
let ex_before31a_2 = before31a({hour = 9; min = 40; period = AM}, {hour = 2; min = 2; period = PM})
let ex_before31a_3 = before31a({hour = 4; min = 40; period = PM}, {hour = 5; min = 50; period = AM})

//3.2 Old british currency 12 pence to a shilling, 20 shillings to a pound

let plus32((p1, s1, d1),(p2, s2, d2)) =
  (p1 + p1 + (s1 + s2) / 20,
    (s1 + s2) % 20 + (d1 + d2) / 12,
    (d1 + d2) % 12)

let subtract32((p1, s1, d1), (p2, s2, d2)) =
  let f = (p1 - p2, s1 - s2, d1 - d2)
  match f with
    | (x, y, z) when x < 0 && y < 0 && z < 0 -> failwith "Not enough money"
    | (x, y, z) when y < 0 && z < 0 -> (x - 1, y + 19, z + 12)
    | (x, y, z) when z < 0 -> (x, y - 1, z + 12)
    | (x, y, z) -> (x, y, z)


let (&+) m1 m2 = plus32(m1, m2)
let (&-) m1 m2 = subtract32(m1, m2)

let ex_plus32_1 = (10,15,8) &+ (10,15,8)
let ex_subtract32_1 = (10,9,8) &- (9,10,11)

type OldBritishCurrency = { pounds: int; shillings: int; pence: int}

let plus32a(c1, c2) =
  { pounds = c1.pounds + c2.pounds + (c1.shillings + c2.shillings) / 20;
    shillings = (c1.shillings + c2.shillings) % 20 + (c1.pence + c2.pence) / 12;
    pence = (c1.pence + c2.pence) % 12}

let subtract32a(c1, c2) =
  let x = {
    pounds = c1.pounds - c2.pounds;
    shillings = c1.shillings - c2.shillings;
    pence = c1.pence - c2.pence}
  match x with
    | {pounds = p; shillings = s; pence = d} when p < 0 && s < 0 && d < 0 ->
      failwith "Not enough money"
    | {pounds = p; shillings = s; pence = d} when s < 0 && d < 0 ->
      {pounds = p - 1; shillings = s + 19; pence = d + 12}
    | {pounds = p; shillings = s; pence = d} when d < 0 ->
      {pounds = p; shillings = s - 1; pence = d + 12}
    | {pounds = p; shillings = s; pence = d} ->
      {pounds = p; shillings = s; pence = d}

let (%+) m1 m2 = plus32a(m1, m2)
let (%-) m1 m2 = subtract32a(m1, m2)

let ex_plus32a_1 =
  {pounds = 10; shillings = 15; pence = 8} %+
  {pounds = 10; shillings = 15; pence = 8}
let ex_subtract32a_1 =
  {pounds = 10; shillings =  9; pence = 8} %-
  {pounds = 9;  shillings = 10; pence = 11}

//3.3

let complex_add((a, b), (c, d)) = (a + c, b + d)

let complex_multiply((a, b), (c, d)) =
  (a *c - b * d, b * c + a * d)

let complex_add_inverse(a, b) =(-a, -b)

let complex_subtract(x,y) =
  complex_add(x, complex_add_inverse(y))

let complex_multiply_inverse(a, b) =
  let absq = a*a + b*b
  (a/absq, -b/absq)

let complex_divide = function
  | (x, y) -> complex_multiply(x, complex_multiply_inverse(y))

let (.+.) x y = complex_add(x, y)
let (.-.) x y = complex_subtract(x, y)
let (.*.) x y = complex_multiply(x, y)
let (./.) x y = complex_divide(x, y)

let ex_complex_add = (4.0, 3.0) .+. (4.0, 2.0)
let ex_complex_subtract = (4.0, 3.0) .-. (4.0, 2.0)
let ex_complex_multiply = (4.0, 3.0) .*. (4.0, 2.0)
let ex_complex_divide = (20.0, 3.0) ./. (4.0, 2.0)

//3.4

type StraightLine = float*float

let mirror_line((m,b) : StraightLine) = (-m, -b)
let string_line = function
  | (m, b) when b < 0.0 -> printfn "y = %gx - %g" m -b
  | (m, b) -> printfn "y = %gx + %g" m b

let ex_mirro_line  = mirror_line(2.0, -1.0)
let ex_string_line_1  = string_line(2.0, -1.0)
let ex_string_line_2  = string_line(mirror_line(2.0, -1.0))

//3.5 - quadratic equation roots solution type

type Solution =
  | NoRoots
  | OneRoot of float
  | TwoRoots of float * float

let solve35(a, b, c) =
  let d = b*b - 4.0*a*c
  if d < 0.0 || a = 0.0
  then NoRoots
  else
    if d = 0.0
    then OneRoot(-b / (2.0*a))
    else
      let sqrtD = sqrt d
      TwoRoots((-b + sqrtD) / (2.0*a), (-b - sqrtD) / (2.0*a))

let ex_solve35_one_root = solve35(1.0, 0.0, 0.0) //x^2
let ex_solve35_two_roots = solve35(1.0, 0.0, -8.0) //x^2 - 8
let ex_solve35_no_roots= solve35(1.0, 0.0, +2.0) //x^2 + 2

//3.7 - shape areas

let area37 = function
  | x when not (isShape x) -> failwith "not a legal shape"
  | Circle r -> System.Math.PI * r * r
  | Square a -> a * a
  | Triangle(a, b, c) ->
    let s = (a + b + c)/2.0
    sqrt(s*(s - a)*(s - b)*(s - c))

let ex_area37_1 = area37(Circle(5.0))

let ex_area37_2 = area37(Square(-2.0))
