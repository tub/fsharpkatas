﻿module Chapter7ExercisesDisplay

open System.Drawing
open System.Windows.Forms
Application.EnableVisualStyles()
open Chapter7Exercises

let winSize = Size(450, 300)

let display(title: string, p: Picture.Picture) =
  let f (x, y) = Point(int(round x), int(p.h) - int(round y))
  let f2 (p1 :Point.Point, p2 :Point.Point) = (f (p1.x, p1.y), f(p2.x, p2.y))
  let pointSegments = Picture.toPointSegments p
  let segmentList = List.map f2 pointSegments
  let pArr = Array.ofList segmentList

  let pen = new Pen(Color.Black)
  let drawLine(g:Graphics) (x: Point, y: Point) = g.DrawLine (pen, x, y)
  let draw(g:Graphics) = List.iter (drawLine g) segmentList

  let panel = new Panel(Dock = DockStyle.Fill)
  panel.Paint.Add(fun e -> draw(e.Graphics))

  let win = new Form(Text=title,
                     Size=winSize,
                     AutoScroll=true,
                     AutoScrollMinSize=Size(int p.w, int p.h))
  win.Controls.Add(panel)
  win.Show()

let go = fun () ->
  let p = Chapter7Exercises.man
  display("Single Man", (Picture.scale 10.0 p))

let go2 = fun () ->
  let p = Chapter7Exercises.couple
  display("Two Men", (Picture.scale 5.0 p))

let go3 = fun () ->
  let p = Chapter7Exercises.crowd
  display("All Men", (Picture.scale 2.6 p))
