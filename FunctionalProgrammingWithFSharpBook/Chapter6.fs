﻿module Chapter6

//--------------
// Chapter 6
//--------------

type Fexpr =
  | Const of float
  | X
  | Add of Fexpr * Fexpr
  | Sub of Fexpr * Fexpr
  | Mul of Fexpr * Fexpr
  | Div of Fexpr * Fexpr
  | Sin of Fexpr
  | Cos of Fexpr
  | Log of Fexpr
  | Exp of Fexpr

let rec D = function
  | Const _ -> Const 0.0
  | X -> Const 1.0
  | Add(fe, ge) -> Add(D fe, D ge)
  | Sub(fe, ge) -> Sub(D fe, D ge)
  | Mul(fe, ge) -> Add(Mul(D fe, fe), Mul(fe, D ge))
  | Div(fe, ge) -> Div(Sub(Mul(D fe, ge), Mul(fe, D ge)), Mul(ge, ge))
  | Sin fe -> Mul(Cos fe, D fe)
  | Cos fe -> Mul(Const -1.0, Mul(Sin fe, D fe))
  | Log fe -> Div(D fe, fe)
  | Exp fe -> Mul(Exp fe, D fe)

let ex_D = D(Mul(Const 3.0, Exp X))

let rec toString = function
  | Const x -> string x
  | X -> "x"
  | Add(fe1, fe2) ->
    "(" + (toString fe1) + ")" +
    " + " + "(" + (toString fe2) + ")"
  | Sub(fe1, fe2) ->
    "(" + (toString fe1) + ")" +
    " - " + "(" + (toString fe2) + ")"
  | Mul(fe1, fe2) ->
    "(" + (toString fe1) + ")" +
    " * " + "(" + (toString fe2) + ")"
  | Div(fe1, fe2) ->
    "(" + (toString fe1) + ")" +
    " / " + "(" + (toString fe2) + ")"
  | Sin fe -> "sin(" + (toString fe) + ")"
  | Cos fe -> "cos(" + (toString fe) + ")"
  | Log fe -> "log(" + (toString fe) + ")"
  | Exp fe -> "exp(" + (toString fe) + ")"

let ex_toString = toString(Mul(Cos(Mul(X, X)),
                               Add(Mul(Const 1.0, X), Mul(X, Const 1.0))))

//Section 6.3
type BinTree<'a, 'b> =
  | Leaf of 'a
  | Node of BinTree<'a, 'b> * 'b * BinTree<'a, 'b>

//Section 6.4
type BinTree2<'a> =
  | Leaf2
  | Node2 of BinTree2<'a> * 'a * BinTree2<'a>

let rec add x t =
  match t with
  | Leaf2 -> Node2(Leaf2, x, Leaf2)
  | Node2(tl, a, tr) when x < a -> Node2(add x tl, a, tr)
  | Node2(tl, a, tr) when x > a -> Node2(tl, a, add x tr)
  | _ -> t

//Section 6.5 Expression Trees
type ExprTree =
  | EConst of int
  | Ident of string
  | Minus of ExprTree
  | Sum of ExprTree * ExprTree
  | Diff of ExprTree * ExprTree
  | Prod of ExprTree * ExprTree
  | Let of string * ExprTree * ExprTree

let et =
  Prod(Ident "a",
       Sum(Minus (EConst 3),
           Let("x", EConst 5, Sum(Ident "x", Ident "a"))))

let rec eval t env =
  match t with
  | EConst n -> n
  | Ident s -> Map.find s env
  | Minus t -> - (eval t env)
  | Sum(t1, t2) -> eval t1 env + eval t2 env
  | Diff(t1, t2) -> eval t1 env - eval t2 env
  | Prod(t1, t2) -> eval t1 env * eval t2 env
  | Let(s, t1, t2) -> let v1 = eval t1 env
                      let env1 = Map.add s v1 env
                      eval t2 env1

let env = Map.add "a" -7 Map.empty
let ex_eval = eval et env

//6.6 Variable sub trees
type ListTree<'a> = LTNode of 'a * (ListTree<'a> list)

let lt7 = LTNode(7, [])
let lt6 = LTNode(6, [])
let lt5 = LTNode(5, [])
let lt4 = LTNode(4, [lt6; lt7])
let lt3 = LTNode(3, [])
let lt2 = LTNode(2, [lt5])
let lt1 = LTNode(1, [lt2; lt3; lt4])

let rec depthFirst (LTNode(x, ts)) =
  x :: (List.collect depthFirst ts)

let rec depthFirstFold f e (LTNode(x, ts)) =
  List.fold (depthFirstFold f) (f e x) ts

let ex_depthFirstFold = depthFirstFold (fun a x -> x::a) [] lt1

let rec breadthFirstList = function
  | [] -> []
  | (LTNode(x, ts))::rest ->
      x :: breadthFirstList(rest@ts)

let ex_breadthFirstList = breadthFirstList [lt1]

//--------------
// Exercises
//--------------

//6.1 Declare a function red to simplify (reduce) the result from D Fexpr
//D(Mul(Const 3.0, Exp X)) =
// Add (Mul (Const 0.0,Const 3.0),Mul (Const 3.0, Mul (Exp X,Const 1.0)))
// which can be simplified as Mul(Const 3.0, Exp X)


let rec red fexp =
  let binRed constr x y =
      let nX = red x
      let nY = red y
      if nX <> x || nY <> y
      then red(constr(nX, nY))
      else constr(red x, red y)

  match fexp with
  | Add(Const x, Const y) -> Const (x + y)
  | Add(Const 0.0, x) | Add(x, Const 0.0) -> red x
  | Add(x, y) -> binRed Add x y
  | Sub(Const x, Const y) -> Const (x - y)
  | Sub(x, Const 0.0) -> red x
  | Sub(x, y) -> binRed Sub x y
  | Mul(Const x, Const y) -> Const (x * y)
  | Mul(x, Const 1.0) | Mul(Const 1.0, x) -> red x
  | Mul(x, Const 0.0) | Mul(Const 0.0, x) -> Const 0.0
  | Mul(Log x, Log y) -> Log(red(Add(x, y)))
  | Mul(x, y) -> binRed Mul x y
  | Div(Const x, Const y) -> Const (x / y)
  | Div(Const 0.0, y) -> Const 0.0
  | Div(x, Const 1.0) -> red x
  | Div(Log x, Log y) -> Log(red(Sub(x, y)))
  | Div(x, y) -> binRed Div x y
  | Sin(x) -> Sin(red x)
  | Cos(x) -> Cos(red x)
  | Log(x) -> Log(red x)
  | Exp(x) -> Exp(red x)
  | Const(x) -> Const(x)
  | X -> X

let ex_red_1 = red(Mul(Const 3.0, Const 2.0))
let ex_red_2 = red(Mul(Const 1.0, Exp X))
let ex_red_3 = red(Mul(Const 0.0, Log X))
let ex_red_4 = red(Mul(Mul(Const 0.0, Sin X), Mul(Const 1.0, Log X)))
let ex_red_5 = red(Mul(Div(Const 0.0, Sin X), Mul(Const 1.0, Log X)))
let ex_red_6 = red(D(Mul(Const 3.0, Exp X)))
let ex_red_7 = red(Mul(Log(Const 7.0), Log(Const 2.0)))

//6.2 postfix form of Fexpressions
let rec postString = function
  | Const x -> string x
  | X -> "x"
  | Add(fe1, fe2) ->
    (postString fe1) + " " +
    (postString fe2) + " + "
  | Sub(fe1, fe2) ->
    (postString fe1) + " " +
    (postString fe2) + " - "
  | Mul(fe1, fe2) ->
    (postString fe1) + " " +
    (postString fe2) + " * "
  | Div(fe1, fe2) ->
    (postString fe1) + " " +
    (postString fe2) + " / "
  | Sin fe -> "(" + (postString fe) + ") sin"
  | Cos fe -> "(" + (postString fe) + ") cos"
  | Log fe -> "(" + (postString fe) + ") log"
  | Exp fe -> "(" + (postString fe) + ") exp"

let ex_postString_1 = postString(Mul(Add(X, Const 7.0), Sub(X, Const(5.0))))
let ex_postString_2 = postString(Mul(Cos(Mul(X, X)),
                                     Add(Mul(Const 1.0, X), Mul(X, Const 1.0))))

//6.3 refined version of toString Fexpr with less parenthesis

//* or / or - need their parmeters in () if they are + or -
//e.g. (a + b) * (c - (d + e))
//e.g  a * sin b * (c / b)
//no need for () with sin, cos, log etc if it's a const or X
//we could use operator precedence to figure it out. If the inner operation
//has higher precedence, you don't need it
let operatorPrecedence = function
  | Const _ | X -> 1
  | Sin _ | Cos _ | Log _ | Exp _ -> 2
  | Add(_,_) -> 3
  | Sub(_,_) -> 4
  | Mul(_,_) | Div(_,_) -> 5

let rec toStringReduced = function
  | Const x -> string x
  | X -> "x"
  | Add(x, y) -> surround(x, Add(x,y)) + " + "  + surround(y, Add(x,y))
  | Sub(x, y) -> surround(x, Sub(x,y)) + " - "  + surround(y, Sub(x,y))
  | Mul(x, y) -> surround(x, Mul(x,y)) + " * "  + surround(y, Mul(x,y))
  | Div(x, y) -> surround(x, Div(x,y)) + " / "  + surround(y, Div(x,y))
  | Sin fe -> "sin" + surround(fe, Sin(fe))
  | Cos fe -> "cos" + surround(fe, Cos(fe))
  | Log fe -> "log" + surround(fe, Log(fe))
  | Exp fe -> "exp" + surround(fe, Exp(fe))

and surround(fexpin,fexpout) =
  if (operatorPrecedence(fexpout) >= operatorPrecedence(fexpin))
  then toStringReduced(fexpin)
  else "(" + toStringReduced(fexpin) + ")"


let ex_toStringReduced_1 = toStringReduced(Add(X, Add(X, Mul(Const 1.0, X))))
let ex_toStringReduced_2 = toStringReduced(Add(Sin(X), Add(Exp(Mul(Const 2.0, X)), Mul(Div(Const 1.0, Log(X)), X))))

//6.4 Binary Trees
let t1 = Node(Node(Leaf 1, "cd", Leaf 2), "ab", Leaf 3)

//6.4.1 Leaf values in a tree
let rec leafVals = function
  | Leaf x -> Set.singleton x
  | Node(l, x, r) -> Set.union(leafVals(l))(leafVals(r))

let ex_leafVals = leafVals t1

//6.4.2 Node values in a tree
let rec nodeVals = function
  | Leaf _ -> Set.empty
  | Node(l, x, r) -> Set.unionMany [nodeVals(l); Set.singleton x; nodeVals(r)]

let ex_nodeVals = nodeVals t1

//6.4.3 Leaf Values and Node Values in a tree
let vals bt =
  (leafVals bt, nodeVals bt)

let ex_vals = vals t1

//6.5 Ancestor Tree
type AncTree =
  | Unspec
  | Info of AncTree * string * AncTree

let ancTree1 =
  Info(Info(
         Info(Unspec, "Mark", Unspec),
         "John",
         Info(Unspec, "Mary", Unspec)),
       "Geroge",
       Info(
         Info(Unspec, "Henry", Unspec),
         "Jane",
         Info(Unspec, "Hillary", Unspec)))

let rec maleAnc = function
  | Unspec -> []
  | Info(Info(gf, f, gm), p, m) -> f::(maleAnc(Info(gf, f, gm)) @ maleAnc m)
  | Info(Unspec, p, m) -> maleAnc m

let ex_maleAnc = maleAnc ancTree1

let rec femaleAnc = function
  | Unspec -> []
  | Info(f, p, Info(gf, m, gm)) -> m::(femaleAnc(Info(gf, m, gm)) @ femaleAnc f)
  | Info(f, p, Unspec) -> femaleAnc f

let ex_femaleAnc = femaleAnc ancTree1

//6.6 Delete an element in a binary tree
let t3 = Node2(Node2(Leaf2, -3, Leaf2), 0, Node2(Leaf2, 2, Leaf2))
let t4 = Node2(t3, 5, Node2(Leaf2, 7, Leaf2))
let t5 = add 4 t4

let rec delete x t =
  match t with
  | Leaf2 -> Leaf2
  | Node2(tl, a, tr) when x < a -> Node2(delete x tl, a, tr)
  | Node2(tl, a, tr) when x > a -> Node2(tl, a, delete x tr)
  | Node2(Leaf2, a, tr) -> tr
  | Node2(tl, a, Leaf2) -> tl
  | Node2(tl, a, tr) ->
    let smallest = findSmallest(tr)
    match smallest with
    | None -> failwith "should be impossible"
    | Some(y) -> Node2(tl, y, delete y tr)
and findSmallest = function
  | Leaf2 -> None
  | Node2(Leaf2, a, _) -> Some(a)
  | Node2(tl, _, _) -> findSmallest(tl)

let ex_delete_1 = delete 7 t5
let ex_delete_2 = delete 8 t5
let ex_delete_3 = delete 0 t5
let ex_delete_4 = delete 2 t5
let ex_delete_5 = delete -3 t5

//6.7 Propositional Logic
//6.7.1 tree representing propositional logic
type Logic =
  | Atom of string
  | Not of Logic
  | And of Logic*Logic
  | Or of Logic*Logic

//6.7.2 Negation Normal Form
let rec nnf = function
  | Atom x -> Atom x
  | Not(Not(x)) -> nnf x
  | Not(And(x, y)) -> Or(nnf(Not(x)), nnf(Not(y)))
  | Not(Or(x, y)) -> And(nnf(Not(x)), nnf(Not(y)))
  | Not(Atom x) -> Not(Atom x)
  | And(x, y) -> And(nnf x, nnf y)
  | Or(x, y) -> Or(nnf x, nnf y)

let ex_nnf_1 = nnf(Not(And(Atom "p", Atom "q")))
let ex_nnf_2 = nnf(Not(Not(Not(Or(Atom "p", Atom "q")))))

//6.7.3 Conjunctive Normal Form
let rec cnf = function
  | Atom x -> Atom x
  | Not x -> Not(cnf x)
  | And(x, y) -> And(cnf x, cnf y)
  | Or(And(x1, x2), y)-> And(cnf(Or(x1, y)), cnf(Or(x2, y)))
  | Or(x, And(y1, y2))-> And(cnf(Or(x, y1)), cnf(Or(x, y2)))
  | Or(x, y) -> Or(cnf x, cnf y)

let ex_cnf_1 = cnf(Or(Atom "p", And(Atom "q", Atom "r")))
let ex_cnf_2 = cnf(Or(And(Atom "p", Atom "s"), And(Atom "q", Atom "r")))

//6.7.4 tautalogy
let rec tautalogy = function
  | Atom x -> false
  | Not x -> tautalogy x
  | Or(Not(x), y) when x = y -> true
  | Or(x, Not(y)) when x = y -> true
  | Or(x, y) when x = y -> false
  | Or(x, y) -> tautalogy(x) || tautalogy(y)
  | And(x, y) -> tautalogy(x) && tautalogy(y)

let ex_tautalogy_1 =
  tautalogy(Not(And(Or(Atom "q", Not(Atom "q")),
                    Or(Or(Atom "p", Not(Atom "p")), Atom "q"))))
let ex_tautalogy_2 = tautalogy(And(Atom "q", Or(Atom "p", Atom "q")))

//6.8 Instruction set calculator
type Instruction =
  | ADD | SUB | MULT | DIV | SIN
  | COS | LOG | EXP  | PUSH of float

//6.8.1 Interpret a single instruction
type Stack = float list

let intpInstr stack instr =
 match (instr, stack) with
 | ADD, a::b::lst -> (b + a)::lst
 | SUB, a::b::lst -> (b - a)::lst
 | MULT, a::b::lst -> (b * a)::lst
 | DIV, a::b::lst -> (b / a)::lst
 | SIN, a::lst -> (sin a)::lst
 | COS, a::lst -> (cos a)::lst
 | LOG, a::lst -> (log a)::lst
 | EXP, a::lst -> (exp a)::lst
 | PUSH p, lst -> p::lst
 | _,_ -> failwith "invalid instruction"

let ex_intpInstr_1 = intpInstr [4.0; 3.0] MULT
let ex_intpInstr_2 = intpInstr [4.0; 3.0] (PUSH 5.0)

//6.8.2 interpret a list of instructions
let intpProg instructions =
   let finalStack = List.fold intpInstr [] instructions
   match finalStack with
   | result::[] -> result
   | _ -> failwith "invalid program. stack did not terminate correctly"

let ex_intpProg_1 = intpProg [(PUSH 5.0); (PUSH 2.0); (MULT)]

//6.8.3 transform a given Fexpr (and value for X) into an instruction list
// hint: postfix form of the expression
let trans (fexpr,value) =
  let rec inner = function
    | Const(x) -> [PUSH x]
    | X -> [PUSH value]
    | Add(x, y) -> inner(x) @ inner(y) @ [ADD]
    | Sub(x, y) -> inner(x) @ inner(y) @ [SUB]
    | Mul(x, y) -> inner(x) @ inner(y) @ [MULT]
    | Div(x, y) -> inner(x) @ inner(y) @ [DIV]
    | Sin(x) -> inner(x) @ [SIN]
    | Cos(x) -> inner(x) @ [COS]
    | Log(x) -> inner(x) @ [LOG]
    | Exp(x) -> inner(x) @ [EXP]
  inner fexpr


let ex_trans_1 = trans(Mul(Const 3.0, Exp X), 2.0)
let ex_intpProg_2 = intpProg ex_trans_1

// (3*4) + (4*2 - 5) = 12 + 3 = 15
let ex_trans_2 = trans(Add(Mul(Const 3.0, X), Sub(Mul(X, Const 2.0), Const 5.0)), 4.0)
let ex_intpProg_3 = intpProg ex_trans_2

//6.9 Company with department heirarchy
//6.9.1, 6.9.2
type Department = Dep of string * float * Department list

//6.9.3 Get List of Deparments (name and gross)
let rec deplist = function
  | Dep(name, gross, []) -> [(name, gross)]
  | Dep(name, gross, subdeps) ->
    (name, gross)::List.fold (fun lst dep -> lst @ (deplist dep)) [] subdeps

let company =
  Dep("A", 32.0, [Dep("B", 20.0, [Dep("D", 6.0, []);
                                  Dep("E", 14.0, [])]);
                  Dep("C", 12.0, [])])

let ex_deplist_1 = deplist(company)

//6.9.4 total income for a department, including sub departments
let rec total_income = function
  | Dep(name, gross, subdeps) ->
    gross + List.fold (fun acc dep -> acc + (total_income dep)) 0.0 subdeps

let ex_total_income_1 = total_income(Dep("X", 20.0, [Dep("Y", 6.0, []); Dep("Z", 14.0, [])]))

//6.9.5 all totals
let rec all_total_income dep =
  let (Dep(name, _, subdeps)) = dep
  (name, total_income(dep))::
    (List.fold(fun acc dep -> all_total_income(dep) @ acc) [] subdeps)

let ex_all_total_income_1 = all_total_income(company)

//6.9.6
let rec format dep =
  let rec print_with_indent indent dep =
    let (Dep(name, gross, subdeps)) = dep
    let str = sprintf "%-3s %6.2f\n" name gross
    (String.replicate indent " ") + str +
      List.fold (fun acc dep -> acc + (print_with_indent (indent + 2) dep)) "" subdeps
  print_with_indent 0 dep

let ex_format_1 = printf "%s" (format company)

//6.10 Extend expression trees to support if then else, and boolean trees
type ExprTreeInt =
  | XConst of int
  | XIdent of string
  | XMinus of ExprTreeInt
  | XSum of ExprTreeInt * ExprTreeInt
  | XDiff of ExprTreeInt * ExprTreeInt
  | XProd of ExprTreeInt * ExprTreeInt
  | XLet of string * ExprTreeInt * ExprTreeInt
  | XIfthenelse of ExprTreeBool * ExprTreeInt * ExprTreeInt
and
  ExprTreeBool =
  | XBool of bool
  | XGreaterThan of ExprTreeInt * ExprTreeInt
  | XGreaterThanEqual of ExprTreeInt * ExprTreeInt
  | XEqual of ExprTreeInt * ExprTreeInt
  | XAnd of ExprTreeBool * ExprTreeBool
  | XOr of ExprTreeBool * ExprTreeBool

let rec xevalint t env =
  match t with
  | XConst n -> n
  | XIdent s -> Map.find s env
  | XMinus t -> - (xevalint t env)
  | XSum(t1, t2) -> xevalint t1 env + xevalint t2 env
  | XDiff(t1, t2) -> xevalint t1 env - xevalint t2 env
  | XProd(t1, t2) -> xevalint t1 env * xevalint t2 env
  | XLet(s, t1, t2) ->
    let v1 = xevalint t1 env
    let env1 = Map.add s v1 env
    xevalint t2 env1
  | XIfthenelse(t1, t2, t3) ->
    let b = xevalbool t1 env
    if b
    then xevalint t2 env
    else xevalint t3 env
and
  xevalbool t env =
  match t with
  | XBool b -> b
  | XGreaterThan(t1, t2) -> xevalint t1 env > xevalint t2 env
  | XGreaterThanEqual(t1, t2) -> xevalint t1 env >= xevalint t2 env
  | XEqual(t1, t2) -> xevalint t1 env = xevalint t2 env
  | XAnd(t1, t2) ->
    let e1 = xevalbool t1 env
    if e1
    then xevalbool t2 env
    else false
  | XOr(t1, t2) ->
    let e1 = xevalbool t1 env
    if e1
    then true
    else xevalbool t2 env

let xenv = Map.ofList [("a",5); ("b", 2); ("c", 1); ("d", 7); ("e", -10)]

let xet =
  XIfthenelse(XAnd(XGreaterThan(XProd(XIdent "a", XConst 3),
                                XSum(XIdent "b", XIdent "c")),
                   XGreaterThan(XIdent "a", XConst 0)),
              XSum(XIdent "c", XIdent "d"),
              XIdent "e")


let ex_xeval = xevalint xet xenv

//6.11 FoldBack version of depthFirstFold
let rec depthFirstFoldBack f (LTNode(x, ts)) e =
  List.foldBack (depthFirstFoldBack f) ts (f x e)

let ex_depthFirstFoldBack = depthFirstFoldBack (fun x a -> x::a) lt1 []

//6.12 I think this question is incorrect. The two functions it wants us to
// implement are already implemented in the text. I think it want's us to
// do breadthFirstFold, since section 6.6 says it's an exercise for the reader

// we could build the breath first list first, then run the fold on it
// or we could do it all in place.. to aovid depth first we need
// to bring back the next level into the current node I think
// We can still do recursive, but we need to pass in more
// state to the next stage. That's going to involve an inner function 
// the inner function will need to run f on the current items, then expand them
let rec breadthFirstFold f e n =
  let rec inner = function
  | (acc, []) -> acc
  | (acc, nodes) ->
    let newState = List.fold f acc (List.map (fun (LTNode(x, _)) -> x) nodes)
    let newNodes = List.collect (fun (LTNode(_, xs)) -> xs) nodes
    inner (newState, newNodes)
  inner (e, [n])

// same as above, but with the list operations condensed into a single fold
let rec breadthFirstFold_f2 f e n =
  let rec inner = function
  | (acc, []) -> acc
  | (acc, nodes) -> inner (List.fold (fun (acc, ns) (LTNode(x, xs)) ->
                                          ((f acc x), ns @ xs))
                                     (acc, []) nodes)
  inner (e, [n])

let ex_breadthFirstFold = breadthFirstFold (fun acc x -> x::acc) [] lt1 

//the thing that makes this much simpler is that we structure it in such a way we
//only have to deal with the contents of a single node at a time. With the two
//above it deals with the whole sublist for each nodes each time.
//(though performance wise it wouldn't surprise me if the above ones are
// slightly faster)
let rec breadthFirstFold_ts f e ts =
  match ts with
  | [] -> e
  | (LTNode(x, xs)::rest) ->
    breadthFirstFold_ts f (f e x) (rest @ xs)

let ex_breadthFirstFold_ts = breadthFirstFold_ts (fun acc x -> x::acc) [] [lt1]


