﻿module Chapter8

open System.Collections.Generic

// 8.2 - Why are the following three declarations excepted by F#?
let mutable a = []
let f x = a <- (x::a)
f(1)

// Because it can figure out that the array must be an integer array
// through following the constrains from the 3rd line, to the 2nd line, to
// the first line


// 8.4 - mutable record types
type t = { mutable link: t; data: int };;

// I'm not sure what the book meant by "declare null to denote the default 
// value of the type of the record type". 'null' is a keyword so we can't
// change it. Lets just use nil.
let mutable nil = Unchecked.defaultof<t>

let linkFirst = { link = nil; data = 53}
let linkSecond = { link = linkFirst; data = 12}
let linkThird = { link = linkSecond; data = 81}

let circularFirst = { link = nil; data = 53}
let circularSecond = { link = circularFirst; data = 12}
let circularThird = { link = circularSecond; data = 81}

circularFirst.link <- circularThird


// 8.5 - gcd function using a while loop and mutable variables
let gcd (xm, xn)= 
  let mutable m = xm
  let mutable n = xn
  while (m <> 0) do 
    let newm = n % m
    n <- m
    m <- newm
  n

let ex_gcd_12_27 = gcd(12, 27) // --> 3
let ex_gcd_36_116 = gcd(36, 116) // --> 4

// 8.6 - fibonacci using a while loop, I wonder if there's a way to make
// this and the above example less verbose
let fibonacci n =
  if n = 1 then
    0
  elif n = 2 then
    1
  else
    let mutable count = 2
    let mutable v1 = 0
    let mutable v2 = 1
    while count < n do
      let pnext = v1 + v2
      v1 <- v2
      v2 <- pnext
      count <- count + 1
    v2

let ex_fibonacci_7 = fibonacci(7) // --> 8
let ex_fibonacci_8 = fibonacci(8) // --> 13

// 8.7 - Hash set fold
let hashSetFold f i (hs:HashSet<'a>) =
  let mutable acc = i
  for v in hs do
    acc <- (f acc v)
  acc

let hs = HashSet<int>()
ignore (hs.Add 4)
ignore (hs.Add 5)
ignore (hs.Add 6)
let ex_87 = hashSetFold (fun acc x -> acc + (sprintf "%d:" x)) ":" hs

// 8.8 - dictionary fold
let dictionaryFold f i (d:Dictionary<'k, 'v>) =
  let mutable acc = i
  for pair in d do
    acc <- (f acc pair.Key pair.Value)
  acc

let dict = Dictionary<string,int>()
ignore (dict.Add ("four", 4))
ignore (dict.Add ("five", 5))
ignore (dict.Add ("six",  6))
let ex_88 = dictionaryFold
              (fun acc k v -> acc + (sprintf " %s:%d *" k v))
              "*" dict

// 8.9 - breadthFirst and breadthFirstFold iterativley

type ListTree<'a> = Node of 'a * (ListTree<'a> list)

let t7 = Node(7, [])
let t6 = Node(6, [])
let t5 = Node(5, [])
let t4 = Node(4, [t6; t7])
let t3 = Node(3, [])
let t2 = Node(2, [t5])
let t1 = Node(1, [t2; t3; t4])

let breadthFirst ltr =
  let remains = Queue<ListTree<'a>>()
  let mutable list = []
  remains.Enqueue ltr
  while (remains.Count <> 0) do
    let (Node (x, tl)) = remains.Dequeue()
    list <- x::list
    List.iter (remains.Enqueue) tl
  List.rev list

let ex_breadthFirst = breadthFirst t1

let breadthFirstFold f xs ltr =
  let remains = Queue<ListTree<'a>>()
  let mutable acc = xs
  remains.Enqueue ltr
  while (remains.Count <> 0) do
    let (Node (x, tl)) = remains.Dequeue()
    acc <- f (acc, x)
    List.iter (remains.Enqueue) tl
  acc

let ex_breadthFirstFold = breadthFirstFold (fun (acc, x) -> x::acc) [] t1
