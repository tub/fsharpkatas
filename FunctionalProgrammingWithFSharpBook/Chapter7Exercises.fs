﻿module Chapter7Exercises

open Chapter7
open Chapter7Display

// 7.1 - Record implementation of the Vector type, using the same signature
// used in the main chapter

module Vector =
  type Vector = {x: float; y: float}

  let make (x, y) = {x= x; y= y}
  let coord {x= x; y= y} = (x, y)

  type Vector with
    static member (~-) {x= x; y= y} = {x= -x; y= -y} 
    static member (+) ({x= x1; y= y1}, {x=x2; y=y2}) = {x= x1+x2; y= y1+y2}
    static member (-) ({x= x1; y= y1}, {x=x2; y=y2})= {x= x1 - x2; y= y1 - y2}
    static member (*) (a, {x= x; y= y}) = {x= a * x; y= a * y}
    static member (*) ({x= x1; y= y1}, {x=x2; y=y2}) = x1 * x2 +  y1 * y2

  let norm {x= x; y= y} = sqrt(x * x + y * y)

// 7.2 Complex number module (using inlines)
//NOTE: all the inline stuff lets generic typing work with numbers and op overload
//      this doesn't work well with signature files though, so I'm not going
//      to use it for this exercise
//module Complex = 
//  type 'a Complex = C of 'a * 'a

//  let make (a, b) = C(a, b)

//  let inline multiply_inverse (C(a,b)) =
//    let absq = a*a + b*b
//    C(a/absq, -b/absq)

//  let inline plus x y =
//    match (x, y) with
//    | (C(a,b), C(c,d)) -> C(a + c, b + d)

//  let inline mult x y =
//    match (x, y) with
//    | (C(a,b), C(c,d)) -> C(a * c - b * d, b * c + a * d)

//  type 'a Complex with
//    static member inline (+) (x, y) = plus x y
//    static member inline (~-) (C(a, b)) = C(-a, -b)
//    static member inline (-) (x, y) = plus x -y
//    static member inline (*) (x, y) = mult x y
//    static member inline (/) (x, y) = mult x (multiply_inverse y)

// 7.2 Complex number module
module Complex = 
  type Complex = C of float * float

  let make (a, b) = C(a, b)
  let toF (C(a, b)) = (a, b)

  let multiply_inverse (C(a,b)) =
    let absq = a*a + b*b
    C(a/absq, -b/absq)

  let plus x y =
    match (x, y) with
    | (C(a,b), C(c,d)) -> C(a + c, b + d)

  let mult x y =
    match (x, y) with
    | (C(a,b), C(c,d)) -> C(a * c - b * d, b * c + a * d)

  type Complex with
    static member (+) (x, y) = plus x y
    static member (~-) (C(a, b)) = C(-a, -b)
    static member (-) (x, y) = plus x -y
    static member (*) (x, y) = mult x y
    static member (/) (x, y) = mult x (multiply_inverse y)

let ex_complex_add = Complex.make(4.0, 3.0) + Complex.make(4.0, 2.0)
let ex_complex_subtract = Complex.make(4.0, 3.0) - Complex.make(4.0, 2.0)
let ex_complex_multiply = Complex.make(4.0, 3.0) * Complex.make(4.0, 2.0)
let ex_complex_divide = Complex.make(20.0, 3.0) / Complex.make(4.0, 2.0)

// 7.3 Module for library of multi-sets of integers represented by weakly
// ascending lists (Ex 4.11)

// I'm using "internal" versions of functions for the recursive functions
// where wrapping it with WeeklyAscending would make things more complex

module WeeklyAscending =
  type WeeklyAscending = WA of int list

  let make (xs) = WA(xs)

  // counts the now many items in the list match the given int
  let rec count = function
    | (WA(x0::xs), x) when x0 = x -> 1 + count(WA(xs), x)
    | (WA(x0::xs), x) when x0 < x -> count(WA(xs), x)
    | _ -> 0

  let rec insert = function
    | (WA([]), x) -> WA([x])
    | (WA(x0::xs), x) when x0 > x -> WA(x::x0::xs)
    | (WA(x0::xs), x) ->
      let (WA(xs0)) = insert(WA(xs), x)
      WA(x0::xs0)

  let rec intersectInternal = function
    | (x0::xs0, x1::xs1) when x0 = x1 -> x0::intersectInternal(xs0, xs1)
    | (x0::xs0, x1::xs1) when x0 < x1 -> intersectInternal(xs0, x1::xs1)
    | (x0::xs0, x1::xs1) when x0 > x1 -> intersectInternal(x0::xs0, xs1)
    | (_, _) -> []

  let intersect (WA(xs0), WA(xs1)) = WA (intersectInternal (xs0, xs1))

  let rec plusInternal = function
    | (x0::xs0, x1::xs1) when x0 <= x1 -> x0::plusInternal(xs0, x1::xs1)
    | (x0::xs0, x1::xs1) -> x1::plusInternal(x0::xs0, xs1)
    | (xs0, xs1) -> xs0 @ xs1

  let plus (WA(xs0), WA(xs1)) = WA (plusInternal (xs0, xs1))

  let rec minusInternal = function
    | (x0::xs0, x1::xs1) when x0 = x1 -> minusInternal(xs0, xs1)
    | (x0::xs0, x1::xs1) when x0 > x1 -> minusInternal(x0::xs0, xs1)
    | (x0::xs0, x1::xs1) -> x0::minusInternal(xs0, x1::xs1)
    | (xs, _) -> xs

  let minus (WA(xs0), WA(xs1)) = WA (minusInternal (xs0, xs1))

  let toList (WA(xs)) = xs

  // Exercise 7.6
  type WeeklyAscending with
    member this.Item
      with get n = count (this, n)


let ex_count_1 = WeeklyAscending.count(WeeklyAscending.make [1; 2; 2; 2; 3], 2)
let ex_count_2 = WeeklyAscending.count(WeeklyAscending.make [1; 2; 2], 2)
let ex_count_3 = WeeklyAscending.count(WeeklyAscending.make [2; 2], 2)
let ex_count_4 = WeeklyAscending.count(WeeklyAscending.make [1], 2)
let ex_count_5 = WeeklyAscending.count(WeeklyAscending.make [4], 2)
let ex_count_6 = WeeklyAscending. count(WeeklyAscending.make [], 2)


let ex_insert_1 = WeeklyAscending.insert(WeeklyAscending.make [], 2)
let ex_insert_2 = WeeklyAscending.insert(WeeklyAscending.make [1], 2)
let ex_insert_3 = WeeklyAscending.insert(WeeklyAscending.make [3], 2)
let ex_insert_4 = WeeklyAscending.insert(WeeklyAscending.make [1; 2; 2; 2; 3; 4; 8], 2)
let ex_insert_5 = WeeklyAscending.insert(WeeklyAscending.make [1; 3; 4; 8], 2)

let ex_intersect =
  WeeklyAscending.intersect (
    WeeklyAscending.make [1; 1; 1; 2; 2; 3; 4],
    WeeklyAscending.make [1; 1; 2; 4; 4])

let ex_plus =
  WeeklyAscending.plus (
    WeeklyAscending.make [1; 1; 2],
    WeeklyAscending.make [1; 2; 4])

let ex_minus_1 =
  WeeklyAscending.minus (
    WeeklyAscending.make [1; 1; 1; 2; 2],
    WeeklyAscending.make [1; 1; 2; 3])
let ex_minus_2 =
  WeeklyAscending.minus (
    WeeklyAscending.make [1; 1; 2; 3],
    WeeklyAscending.make [1; 1; 1; 2; 2])

let ex_2s_count = (WeeklyAscending.make [1; 2; 2; 2; 3; 4; 8]).[2]

// Exercise 7.4 - Sig and implementation for a library of polynomials with
// integer coefficients (See Exercise 4.22)

module Poly =
  type Poly =
    P of int list
      with
        //make print a member so we can use it in ToString
        member p.print() = 
          let (P(poly))  = p
          let print_plus = function
            | false -> ""
            | _ -> " + "

          let print_x = function
            | 0 -> ""
            | 1 -> "x"
            | n -> sprintf "x^%d" n

          let print_n = function
            | 0 -> ""
            | n -> (sprintf "%d" n)

          let rec print_self = function
            | (k, i, []) -> ""
            | (k, i, 0::ys) -> print_self(k, i+1, ys) //don't print anything with a 0 scaler
            | (k, i, y::ys) -> print_plus(k) + print_n(y) + print_x(i) + print_self(true, i+1, ys)
          print_self (false, 0, poly)
        // Exercise 7.5 - ToSTring
        override p.ToString() = p.print()

  let make xs = P(xs)
  let coef (P(xs)) = xs

  let rec scalarMultiplyRec s = function
    | [] -> []
    | x::xs -> x*s::(scalarMultiplyRec s xs)
  let scalarMultiply s (P(xs)) = P(scalarMultiplyRec s xs)

  //multiply a polynomial by x
  //so 1 + 2x + 3x^2
  //becomes 0 + 1x + 2x^2 + 3x^3
  //or [1; 2; 3] -> [0; 1; 2; 3]
  let multiplyByXInternal poly = [0] @ poly
  let multiplyByX (P(poly)) = P(multiplyByXInternal poly)

  //add two polys
  //so (1 + 2x) + (2 + 3x + 8x^2)
  //becomes (3 + 5x + 8x^2)
  let rec addRec polyA polyB =
    match (polyA, polyB) with
    | ([],[]) -> []
    | (xs, []) -> xs
    | ([], xs) -> xs
    | (x::xs, y::ys) -> (x+y)::(addRec xs ys)

  let add (P(xs0)) (P(xs1)) = P(addRec xs0 xs1)

  //multiply one polynomial by another
  //so (4 + 2x) * (3 + x)
  //becomes
  // (4*(3 + x) + 2x*(3 + x)
  //= 12 + 4x + 6x + 2x^2
  //= 12 + 10x + 2x^2
  let rec multiplyPolyRec poly = function
    | [] -> []
    | x::xs ->
      let a = scalarMultiplyRec x poly
      let b = multiplyByXInternal (multiplyPolyRec xs poly)
      addRec a b

  let multiplyPoly (P(poly1)) (P(poly2)) = P(multiplyPolyRec poly1 poly2)

  //trying to do this fully recursive
  //...it would be a lot easier with string.join etc

  type Poly with
    static member (*) (s, poly) = scalarMultiply s poly
    static member (*) (p1, p2) = multiplyPoly p1 p2
    static member (!*) (poly) = multiplyByX poly
    static member (+) (x, y) = add x y

    // Exercise 7.7 - poly.[n] returns the coefficient for x^n
    member p.Item
      with get n =
        match (n, p) with
          | (n, P(xs)) when n >= List.length xs -> 0
          | (n, _) when n < 0 -> 0
          | (n, P(xs)) -> List.item n xs


let ex_poly_mult = 5 * (Poly.make [2; 0; 3; 1])

let ex_poly_mult_x = !* (Poly.make [1; 2; 3])

let ex_poly_add = (Poly.make [1; 2]) + (Poly.make [2; 3; 8])

let ex_poly_mult_poly = (Poly.make [4; 2]) * (Poly.make [3; 1])

let ex_poly_print_1 = (Poly.make [4; 0; 2; 8]).ToString()
let ex_poly_print_2 = (Poly.make [0; 0; 2; 8]).ToString()
let ex_poly_print_3 = (Poly.make [0; 3; 2; 8]).ToString()

// Exercise 7.8 - Drawing sierpinski curves
let sierpinski hn =
  let w = Curve.width hn
  let h = Curve.height hn
  let c1 = hn
  let c2 = (c1 |^ 90) --> (w, h + 1.0)
  let c3 = (c1 |^ -90) --> (w - h + 1.0, h + w + 2.0)
  let c4 = c1 --> (w + 2.0, h + 2.0)
  c1 + c2 + c3 + c4

let drawSierpinski = fun () ->
  let s0 = Curve.point (0.0, 0.0)
  let s1 = sierpinski s0
  let s2 = sierpinski s1
  let s3 = sierpinski s2
  let s4 = sierpinski s3
  Chapter7Display.display("Sierpinski 4", adjust(s4, 10.0))

// Exercise 7.9 - Drawing peano curves
let peanoCurve hn =
  let w = Curve.width hn
  let h = Curve.height hn

  let p1 = hn
  let p2 = (p1 >< 0.0) --> (w, h + 1.0)
  let p3 = p1 --> (0.0, 2.0*h + 2.0)
  let p1to3 = p1 + p2 + p3

  //4-6 are 1-3 rotated and moved up and right
  let p4to6 = (p1to3 |^ 180 >< 0.0) --> (w + 1.0, 3.0*h + 2.0)

  //7-9 are 1-3 moved right
  let p7to9 = p1to3 --> (2.0*w + 2.0, 0.0)

  p1to3 + p4to6 + p7to9

let drawPeano = fun () ->
  let p0 = Curve.point (0.0, 0.0)
  let p1 = peanoCurve p0
  let p2 = peanoCurve p1
  let p3 = peanoCurve p2
  let p4 = peanoCurve p3
  Chapter7Display.display("Peano 4", adjust(p4, 10.0))

// Exercise 7.10 - Not quite sure what the book means by opposite order?

// Exercise 7.11 - Picture

module Point =
  type Point = P of float * float

  let make (x, y) = P(x, y)
  let scale c (P(x, y)) = P(x * c, y  * c)
  let add (P(x1, y1)) (P(x2, y2)) = P(x1 + x2, y1 + y2)

  type Point with
    member p.x =
      let (P(x, _)) = p
      x

    member p.y =
      let (P(_, y)) = p
      y
    static member (*) (c, p) = scale c p
    static member (+) (p1, p2) = add p1 p2

module Segment =
  type Segment = S of Point.Point * Point.Point

  let make (p1, p2) = S(p1, p2)
  let toPoints (S(p1, p2)) = (p1, p2)
  let map f (S(p1, p2)) = S(f p1, f p2)

  let scale c (S(p1, p2)) =
    S(c * p1, c * p2)

  let offset (x, y) (S(p1, p2)) =
    let point = Point.make(x, y)
    S(p1 + point, p2 + point)

  type Segment with
    member s.p1 =
      let (S(p1, _)) = s
      p1
    member s.p2 =
      let (S(_, p2)) = s
      p2

module LinkedSegment =
  type LinkedSegment = L of Segment.Segment list

  let make (xs) = L(xs)

  let toSegments (L(xs)) = xs

  let append p (L(l)) = 
    match l with
    | [] -> failwith "Cannot append to an empty LinkedSegment"
    | x::xs ->
      let s = Segment.make (x.p2, p)
      make (s::x::xs)
    

module Picture =
  type Picture = P of float * float * Segment.Segment list

  let grid w h segments =
    P(w, h, segments)

  let scale c (P(w, h, segments)) =
    let scaledSegments = List.map (Segment.scale c) segments
    P(w * c, h * c, scaledSegments)

  let scaleAndOffset c offset (P(w, h, segments)) =
    let (P(sw, sh, ss)) = scale c (P(w, h, segments)) 
    P(sw, sh, (List.map (Segment.offset offset) ss))

  let flip (P(w, h, segments)) = 
    let flipPoint (p:Point.Point) = Point.make(w - p.x, p.y)
    let flipSegment s = Segment.map flipPoint s
    let flippedSegments = List.map flipSegment segments
    (P(w, h, flippedSegments))

  let beside (P(w1, h1, ss1)) (P(w2, h2, ss2)) = 
    let scalingFactor = h1/h2
    let (P(sw, sh, scaledAndOffsetSegments)) =
      scaleAndOffset scalingFactor (w1, 0.0) (P(w2, h2, ss2))
    P(w1 + sw, h1, ss1 @ scaledAndOffsetSegments)

  let above (P(w1, h1, ss1)) (P(w2, h2, ss2)) = 
    let scalingFactor = w2/w1
    let (P(sw, sh, scaledAndOffsetSegments)) =
      scaleAndOffset scalingFactor (0.0, h2) (P(w1, h1, ss1))
    P(w2, h2 + sh, ss2 @ scaledAndOffsetSegments)

  let row n p =
    List.reduce beside (List.replicate n p)

  let column n p =
    List.reduce above (List.replicate n p)

  let toPointSegments (P(_, _, xs)) =
    List.map Segment.toPoints xs

  type Picture with
    member p.w =
      let (P(w, _, _)) = p
      w
    member p.h =
      let (P(_, h, _)) = p
      h

let manSegments = 
  LinkedSegment.make [
    Segment.make (Point.make (4.0, 0.0), Point.make (6.0, 8.0)) ]
  |> LinkedSegment.append (Point.make ( 6.0, 10.0))
  |> LinkedSegment.append (Point.make ( 0.0, 10.0))
  |> LinkedSegment.append (Point.make ( 0.0, 12.0))
  |> LinkedSegment.append (Point.make ( 6.0, 12.0))
  |> LinkedSegment.append (Point.make ( 6.0, 14.0))
  |> LinkedSegment.append (Point.make ( 4.0, 16.0))
  |> LinkedSegment.append (Point.make ( 4.0, 18.0))
  |> LinkedSegment.append (Point.make ( 6.0, 20.0))
  |> LinkedSegment.append (Point.make ( 8.0, 20.0))
  |> LinkedSegment.append (Point.make (10.0, 18.0))
  |> LinkedSegment.append (Point.make (10.0, 16.0))
  |> LinkedSegment.append (Point.make ( 8.0, 14.0))
  |> LinkedSegment.append (Point.make ( 8.0, 12.0))
  |> LinkedSegment.append (Point.make (10.0, 12.0))
  |> LinkedSegment.append (Point.make (10.0, 14.0))
  |> LinkedSegment.append (Point.make (12.0, 14.0))
  |> LinkedSegment.append (Point.make (12.0, 10.0))
  |> LinkedSegment.append (Point.make ( 8.0, 10.0))
  |> LinkedSegment.append (Point.make ( 8.0, 8.0))
  |> LinkedSegment.append (Point.make (10.0, 0.0))
  |> LinkedSegment.append (Point.make ( 8.0, 0.0))
  |> LinkedSegment.append (Point.make ( 7.0, 4.0))
  |> LinkedSegment.append (Point.make ( 6.0, 0.0))
  |> LinkedSegment.append (Point.make ( 4.0, 0.0))

let man = Picture.grid 14.0 20.0 (LinkedSegment.toSegments manSegments)
let couple = Picture.beside (Picture.flip man) man

let crowd =
  [0..6] |> List.map (fun n -> Picture.row (n + 6) man)
         |> List.rev
         |> List.mapi (fun i p -> if i % 2 = 0 then p else Picture.flip p )
         |> List.reduce Picture.above 
