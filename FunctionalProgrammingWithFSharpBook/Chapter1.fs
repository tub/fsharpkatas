﻿module Chapter1

//--------------
// Chapter 1
//--------------

let rec fact = function
  | 0 -> 1
  | n -> n * fact(n - 1)

let eg_fact_4 = fact 4
let eg_fact_6 = fact 6

let rec power = function
  | (x, 0) -> 1.0
  | (x, n) -> x * power(x, n - 1)

let eg_power_4_2 = power (4.0, 2)
let eg_power_5_5 = power (5.0, 5)

let rec gcd = function
  | (0, n) -> n
  | (m, n) -> gcd(n % m, m)

let ex_gcd_12_27 = gcd(12, 27) // --> 3
let ex_gcd_36_116 = gcd(36, 116) // --> 4

// Chapter 1 - Exercises

//1.1
let g11(n) = n + 4
let eg_g11_5 = g11(5)

//1.2 -- note: this gets quite confused with the typing
//pythagorean theorem
let h12(x:float, y:float) = System.Math.Sqrt(x*x + y*y)
let eg_h12_5_2 = h12(5.0, 2.0)

//1.3 - the above as function expressions
let g13 = function
  | n -> n + 4

let h13 = function
  | (x, y) -> sqrt(x*x + y*y)

let eg_g13_5 = g13(5)
let eg_h13_5_2 = h13(5.0, 2.0)

//1.4 - 1 + 2 + ... + (n-1) + n
let rec f14 = function
  | 0 -> 0
  | n -> n + f14(n - 1)

let eg_f14_6 = f14(6)

//1.5
let rec fibonacci = function
  | 0 -> 0
  | 1 -> 1
  | n -> fibonacci(n - 1) + fibonacci(n - 2)

let ex_fibonacci_7 = fibonacci(7) // --> 8
let ex_fibonacci_8 = fibonacci(8) // --> 13

//1.6 - m + (m + 1) + (m + 2) + .... (m + n - 1) + (m + n)
//simplified =  m*n + (1 + 2 + ... (n - 1) + n)
let rec sum16 = function
  | (m, 0) -> 0
  | (m, n) -> m + n + sum16(m, n - 1)

let ex_sum16 = sum16(5, 6)

//1.7  - Determine Type
// (System.Math.PI, fact - 1) = (float*int)
// fact(fact 4) = (int -> int) -> int
// power(System.Math.PI, fact 2) = (float * int) -> float
// (power, fact) = ((float * int) -> float, int -> int)

//1.8
let a18 = 5
let f18 a18 = a18 + 1
let g18 b18 = (f18 b18) + a18;

let ex_f18_3 = f18 3 // --> 4
let ex_g18_5 = g18 5 // --> 11

