﻿module Chapter4

//--------------
// Chapter 4
//--------------

let rec altsum_orig = function
  | [] -> 0
  | [x] -> x
  | x0::x1::xs -> x0 - x1 + altsum_orig xs;

let rec naiveRev = function
  | [] -> []
  | x::xs -> naiveRev xs @ [x]

let rec isMember x = function
  | y::ys -> x = y || (isMember x ys)
  | [] -> false

type Country = string
type Map = (Country * Country) list
type Colour = Country list
type Colouring = Colour list

// decides whether two countries are neighbours
let areNb map c1 c2 =
  isMember (c1, c2) map || isMember (c2, c1) map

// decides whether a colour can be extended by a country
let rec canBeExtBy map clr cntry =
  match clr with
  | [] -> true
  | cntry'::clr' ->
    not(areNb map cntry' cntry) && canBeExtBy map clr' cntry

// extends a colouring by an extra country
let rec extColouring map clrs cntry =
  match clrs with
  | [] -> [[cntry]]
  | clr::clrs' ->
    if canBeExtBy map clr cntry
    then (cntry::clr)::clrs'
    else clr::extColouring map clrs' cntry

let addElem x ys = if isMember x ys then ys else x::ys

// computes a list of countries in a map
let rec countries = function
  | [] -> []
  | (c1,c2)::map -> addElem c1 (addElem c2 (countries map))

// builds a colouring for a list of countries
let rec buildColouring map = function
  | [] -> []
  | c::cs -> extColouring map (buildColouring map cs) c

let colourMap map = buildColouring map (countries map)

// Exercises

//4.1
let upto(n) =
  [1..n]

let eg_upto = upto(5)

//4.2
let downto1(n) =
  [n .. -1 .. 1]

let eg_downto1 = downto1(5)

//4.3
let evenN(n) =
  [2 .. 2 .. n]

let eg_evenN = evenN(15)

//4.4
let rec altsum = function
  | [] -> 0
  | x::xs -> x - altsum xs;
//   x0 - (x1 - (x2 - (x3 - ...
// = x0 -  x1 + (x2 - (x3 -
// = x0 -  x1 +  x2 - (x3 -

let eg_altsum = altsum [2; -1; 3]

//4.5
let rec rmodd = function
  | x0::x1::xs -> x1::(rmodd xs);
  | _ -> []

let eg_rmodd_1 = rmodd [13; 23; 35; 45; 57; 67]
let eg_rmodd_2 = rmodd [13; 23; 35; 45; 57; 67; 79]

//4.6
let rec remEvenInt(list: int list) =
  match list with
  | x0::x1::xs -> x0::(remEvenInt xs);
  | _ -> []

let eg_remEvenInt_1 = remEvenInt [13; 23; 35; 45; 57; 67]
let eg_remEvenInt_2 = remEvenInt [13; 23; 35; 45; 57; 67; 79]

//4.7
let rec multiplicity n = function
  | [] -> 0
  | x::xs when x = n -> 1 + multiplicity n xs
  | x::xs -> multiplicity n xs

let eg_multiplicity = multiplicity 4 [1; 4; 3; 4; 4; 8]

//4.8
let rec split = function
  | x0::x1::xs ->
    let (l0, l1) = split(xs)
    (x0::l0, x1::l1)
  | x::xs -> failwith "Lists aren't of equal length"
  | _ -> ([], [])

let eg_split = split [13; 23; 35; 45; 57; 67]

//4.9
let rec zip l0 l1 =
  match (l0, l1) with
  | (x0::xs0, x1::xs1) -> (x0, x1)::(zip xs0 xs1)
  | ([], []) -> []
  | _ -> failwith "Lists aren't of equal length"

let eg_zip = zip [1; 2; 3; 4; 5] [9; 8; 7; 6; 5]

//4.10
let rec prefix lst0 lst1 =
  match (lst0, lst1) with
  | (x0::xs0, x1::xs1) ->
    if x0 = x1
    then prefix xs0 xs1
    else false
  | ([], _) -> true
  | _ -> false

let eg_prefix_0 = prefix [] [1]
let eg_prefix_1 = prefix [1] []
let eg_prefix_2 = prefix [1; 2; 3;] [1; 2; 3]
let eg_prefix_3 = prefix [1; 2; 3;] [1; 2; 4]
let eg_prefix_4 = prefix [1; 2; 3;] [1; 2;]
let eg_prefix_5 = prefix [1; 2;] [1; 2; 3]

//4.11
let rec count = function
  | (x0::xs, x) when x0 = x -> 1 + count(xs, x)
  | (x0::xs, x) when x0 < x -> count(xs, x)
  | _ -> 0

let ex_count_1 = count([1; 2; 2; 2; 3], 2)
let ex_count_2 = count([1; 2; 2], 2)
let ex_count_3 = count([2; 2], 2)
let ex_count_4 = count([1], 2)
let ex_count_5 = count([4], 2)
let ex_count_6 = count([], 2)

let rec insert = function
  | ([], x) -> [x]
  | (x0::xs, x) when x0 > x -> x::x0::xs
  | (x0::xs, x) -> x0::insert(xs, x)

let ex_insert_1 = insert([], 2)
let ex_insert_2 = insert([1], 2)
let ex_insert_3 = insert([3], 2)
let ex_insert_4 = insert([1; 2; 2; 2; 3; 4; 8], 2)
let ex_insert_5 = insert([1; 3; 4; 8], 2)

let rec intersect = function
  | (x0::xs0, x1::xs1) when x0 = x1 -> x0::intersect(xs0, xs1)
  | (x0::xs0, x1::xs1) when x0 < x1 -> intersect(xs0, x1::xs1)
  | (x0::xs0, x1::xs1) when x0 > x1 -> intersect(x0::xs0, xs1)
  | (_, _) -> []

let ex_intersect = intersect([1; 1; 1; 2; 2; 3; 4], [1; 1; 2; 4; 4])

let rec plus = function
  | (x0::xs0, x1::xs1) when x0 <= x1 -> x0::plus(xs0, x1::xs1)
  | (x0::xs0, x1::xs1) -> x1::plus(x0::xs0, xs1)
  | (xs0, xs1) -> xs0 @ xs1

let ex_plus = plus([1; 1; 2], [1; 2; 4])

let rec minus = function
  | (x0::xs0, x1::xs1) when x0 = x1 -> minus(xs0, xs1)
  | (x0::xs0, x1::xs1) when x0 > x1 -> minus(x0::xs0, xs1)
  | (x0::xs0, x1::xs1) -> x0::minus(xs0, x1::xs1)
  | (xs, _) -> xs

let ex_minus_1 = minus([1; 1; 1; 2; 2], [1; 1; 2; 3])
let ex_minus_2 = minus([1; 1; 2; 3], [1; 1; 1; 2; 2])

//4.12
let rec sum = function
  | (p, x::xs) ->
    if p(x)
    then x + sum(p, xs)
    else sum(p, xs)
  | (_, []) -> 0

let ex_sum = sum((fun x -> x > 2), [1; 2; 3; 4])

//4.13
let rec smallest = function
  | x0::x1::xs ->
    if x0 < x1
    then smallest(x0::xs)
    else smallest(x1::xs)
  | x0::[] -> x0
  | [] -> failwith "There cannot be a smallest element in an empty list"

let ex_smallest = smallest([5; 2; 3; 6; 4; 9; 8])

let rec delete = function
  | (a, x::xs) when a = x -> xs
  | (a, x::xs) -> x::delete(a, xs)
  | _ -> []

let ex_delete_1 = delete(2, [5; 2; 3; 2; 4; 2; 8])
let ex_delete_2 = delete(9, [5; 2; 3; 2; 4; 2; 8])

let rec sort = function
  | [] -> []
  | xs ->
    let next = smallest(xs)
    let rest = delete(next, xs)
    next::sort(rest)

let ex_sort = sort([5; 2; 3; 2; 4; 8; 2])

//4.14
let rec smallest_opt = function
  | x0::x1::xs ->
    if x0 < x1
    then smallest_opt(x0::xs)
    else smallest_opt(x1::xs)
  | x0::[] -> Some(x0)
  | [] -> None

let ex_smallest_opt_1 = smallest_opt([5; 2; 3; 6; 4; 9; 8])
let ex_smallest_opt_2 = smallest_opt(([]:int list))

//4.15
let rec revrev = function
  | [] -> []
  | x::lst -> revrev(lst) @ [(naiveRev x)]

let ex_revrev = revrev [[1; 2]; [3; 4; 5]]

//4.16
let rec f = function
  | (x, []) -> []
  | (x, y::ys) -> (x+y)::f(x-1, ys)
// (f) adds a steadily decreasing value to each element of the list
// type: int * int list -> int list

let rec g = function
  | [] -> []
  | (x,y)::s -> (x,y)::(y,x)::g s
// (g) takes a list of tuples, and doubles up each tuple with a reversed version
// type: ('a * 'a) list -> ('a * 'a) list

let rec h = function
  | [] -> []
  | x::xs -> x::(h xs)@[x]
// (h) takes a list, and adds a reversed version onto the end
// type: 'a list -> 'a list

//4.17
let rec p q = function
  | [] -> []
  | x::xs ->
    let ys = p q xs
    if q x then x::ys else ys@[x]
// (p) takes a predicate function and a list. Recusivly applies itself
// to the tail of the list. The predicate function decides if the item
// at the head of the list should stay there, or move to the end.
// type: ('a -> bool) -> 'a list -> 'a list

let ex_p = p (fun x -> x % 2 = 0) [2; 3; 1; 4; 8; 5; 7]

//4.18
let fec f2 g = function
  | [] -> []
  | x::xs -> g x :: f2 (fun y -> g(g y)) xs

// (f2) takes a predicate function and a list
// Applies the predicate function to the first member, then
// cons another application of (f2) with the predicate function changed
// to be double operation of itself
// type: ('a -> 'a) -> 'a list -> 'a list

//4.19
//let areNb map c1 c2 =
//  isMember (c1, c2) map || isMember (c2, c1) map
// may traverse the list twice because isMember needs to go through
// the same list twice to check each match

//fix it by inlining isMember...
//for a more general solution we could change isMember to use a predicate
let rec areNb_improved map c1 c2 =
  match map with
  | (x,y)::xs -> (x,y) = (c1,c2) || (x,y) = (c2,c1) || (areNb_improved xs c1 c2)
  | [] -> false

//4.20 - remove extranious map references
let rec colourMap_ext map =
  let areNb c1 c2 =
    isMember (c1, c2) map || isMember (c2, c1) map

  let rec canBeExtBy clr cntry =
    match clr with
    | [] -> true
    | cntry'::clr' ->
      not(areNb cntry' cntry) && canBeExtBy clr' cntry

  let rec extColouring clrs cntry =
    match clrs with
    | [] -> [[cntry]]
    | clr::clrs' ->
      if canBeExtBy clr cntry
      then (cntry::clr)::clrs'
      else clr::extColouring clrs' cntry

  let rec buildColouring = function
    | [] -> []
    | c::cs -> extColouring (buildColouring cs) c

  buildColouring (countries map)

let ex_colourMap = colourMap_ext [("a", "b"); ("c", "d"); ("d", "a")]

//4.21 -- allow for islands
//assuming islands are represented by a country paired with
//an empty string, then all we need to do to support them is
//not include them in the countries list we build at the beginning
let rec colourMap_islands map =
  let areNb c1 c2 =
    isMember (c1, c2) map || isMember (c2, c1) map

  let rec canBeExtBy clr cntry =
    match clr with
    | [] -> true
    | cntry'::clr' ->
      not(areNb cntry' cntry) && canBeExtBy clr' cntry

  let rec extColouring clrs cntry =
    match clrs with
    | [] -> [[cntry]]
    | clr::clrs' ->
      if canBeExtBy clr cntry
      then (cntry::clr)::clrs'
      else clr::extColouring clrs' cntry

  let rec buildColouring = function
    | [] -> []
    | c::cs -> extColouring (buildColouring cs) c

  let rec countries = function
    | [] -> []
    | (c1, "")::map -> addElem c1 (countries map)
    | ("", c1)::map -> addElem c1 (countries map)
    | (c1,c2)::map -> addElem c1 (addElem c2 (countries map))

  buildColouring (countries map)

let ex_colourMap_islands = colourMap_islands [("a", "b"); ("c", "d"); ("d", "a"); ("e", ""); ("f", "")]

//4.22
//poly = a0 + a1*x + a2*x^2 + ... + an*x^n
//represented as a list of coeficients
//[a0; a1; a2; ... ; an]
//multiply a polynomial by a constant
let rec poly_mult s = function
  | [] -> []
  | x::xs -> x*s::poly_mult s xs

let ex_poly_mult = poly_mult 5 [2; 0; 3; 1]

//multiply a polynomial by x
//so 1 + 2x + 3x^2
//becomes 0 + 1x + 2x^2 + 3x^3
//or [1; 2; 3] -> [0; 1; 2; 3]
let poly_mult_x poly =
  [0] @ poly

let ex_poly_mult_x = poly_mult_x [1; 2; 3]

//add two polys
//so (1 + 2x) + (2 + 3x + 8x^2)
//becomes (3 + 5x + 8x^2)
let rec poly_add polyA polyB =
  match (polyA, polyB) with
  | ([],[]) -> []
  | (xs, []) -> xs
  | ([], xs) -> xs
  | (x::xs, y::ys) -> (x+y)::poly_add xs ys

let ex_poly_add = poly_add [1; 2] [2; 3; 8]

let (&*) s poly = poly_mult s poly

let (~%%) poly = poly_mult_x poly

let (%+) polyA polyB = poly_add polyA polyB

//multiply one polynomial by another
//so (4 + 2x) * (3 + x)
//becomes
// (4*(3 + x) + 2x*(3 + x)
//= 12 + 4x + 6x + 2x^2
//= 12 + 10x + 2x^2
let rec poly_mult_poly poly = function
  | [] -> []
  | x::xs ->  (x &* poly) %+ %%(poly_mult_poly poly xs)

//todo - infix operator for poly_mult_x

let ex_poly_mult_poly = poly_mult_poly [4; 2] [3; 1]

//trying to do this fully recursive
//...it would be a lot easier with string.join etc
let poly_print poly =
  let print_plus = function
    | false -> ""
    | _ -> " + "

  let print_x = function
    | 0 -> ""
    | 1 -> "x"
    | n -> sprintf "x^%d" n

  let print_n = function
    | 0 -> ""
    | n -> (sprintf "%d" n)

  let rec print = function
    | (k, i,[]) -> ""
    | (k, i, 0::ys) -> print(k, i+1, ys) //don't print anything with a 0 scaler
    | (k, i, y::ys) -> print_plus(k) + print_n(y) + print_x(i) + print(true, i+1, ys)
  print (false, 0, poly)

let ex_poly_print_1 = poly_print [4; 0; 2; 8]
let ex_poly_print_2 = poly_print [0; 0; 2; 8]
let ex_poly_print_3 = poly_print [0; 3; 2; 8]

//4.23 - dating service
type Sex = Male | Female
type file = { name: string; ph: string; sex: Sex ; yob: int; themes: string list }

let client1 = { name = "Bob"; ph = "555";
  sex = Male; yob = 1992; themes = ["walks"; "sailing"]}
let client2 = { name = "Joe"; ph = "555";
  sex = Male; yob = 1987; themes = ["partying"; "football"]}
let client3 = { name = "Jenny"; ph = "555";
  sex = Female; yob = 1996; themes = ["partying"; "shopping"]}
let client4 = { name = "Stacy"; ph = "555";
  sex = Female; yob = 1998; themes = ["partying"; "sailing"]}
let client5 = { name = "Mary"; ph = "555";
  sex = Female; yob = 1981; themes = ["partying"; "walks"]}

let rec find_matches client allClients =
  let rec themes_match = function //efficient? not one bit!
    | (x::xs, y::ys) ->
      if x = y
      then true
      else themes_match(xs, y::ys) || themes_match(x::xs, ys)
    | (_,_) -> false

  let check_match (clientA, clientB) =
    clientA.sex <> clientB.sex //hetro only apparently
    && (abs(clientA.yob - clientB.yob) < 10)
    && themes_match(clientA.themes, clientB.themes)

  match allClients with
  | [] -> []
  | x::xs ->
    if check_match(client, x)
    then x::find_matches client xs
    else find_matches client xs

let ex_find_matches_1 = find_matches client1 [client1; client2; client3; client4; client5]
let ex_find_matches_2 = find_matches client2 [client1; client2; client3; client4; client5]
let ex_find_matches_3 = find_matches client3 [client1; client2; client3; client4; client5]
let ex_find_matches_4 = find_matches client4 [client1; client2; client3; client4; client5]
let ex_find_matches_5 = find_matches client5 [client1; client2; client3; client4; client5]
