﻿module Chapter5

//--------------
// Chapter 5
//--------------

type ArticleCode = string
type ArticleName = string
type NoPieces = int
type Price = int

type Register = Map<ArticleCode, ArticleName * Price>

type Info = NoPieces * ArticleName * Price
type Infoseq = Info list
type Bill = Infoseq * Price

type Item = NoPieces * ArticleCode

let reg1 = Map.ofList [("a1", ("cheese", 25));
                       ("a2", ("herring", 4));
                       ("a3", ("softdrink", 5))]

let makeBill2 register purchase =
  let f (noPieces, articleCode) (infos, billPrice) =
    let (articleName, articlePrice) = Map.find articleCode register
    let linePrice = noPieces * articlePrice
    ((noPieces, articleName, linePrice)::infos, linePrice + billPrice)
  List.foldBack f purchase ([], 0)

let purchaseList = [(3, "a2"); (1, "a1")]
let ex_makeBill2 = makeBill2 reg1 purchaseList

type Purchase = Map<ArticleCode, NoPieces>

let makeBill3 register purchase =
  let f articleCode noPieces (infos, billPrice) =
    let (articleName, articlePrice) = Map.find articleCode register
    let linePrice = noPieces * articlePrice
    ((noPieces, articleName, linePrice)::infos, linePrice + billPrice)
  Map.foldBack f purchase ([], 0)

let purchaseMap = Map.ofList [("a2", 3); ("a1", 1)]
let ex_makeBill3 = makeBill3 reg1 purchaseMap

//--------------
// Exercises
//--------------

// the book has completly ignored the pipe "|>" operator so far, so I'm doing
// the same, though it does make things a bit harder to read

// I think quite a few things would be easier with unfold as well,
// but the book hasn't covered taht yet...

//5.1 - filter using foldBack
let filter pred lst =
  List.foldBack (fun next acc -> if pred(next) then next::acc else acc) lst []

let ex_filter = filter (fun x -> x % 2 = 0) [1; 2; 3; 4; 5; 6; 7; 8; 9; 10]

//5.2 - solve 4.15 using a fold
//a little messy with the double fold.. easy to clean up with an inner function
let rec revrev lst =
  List.fold (fun acc x -> (List.fold (fun acc2 y -> y::acc2) [] x)::acc) [] lst

let ex_revrev = revrev [[1; 2]; [3; 4; 5]]

//5.3 - solve 4.12 using a fold
let rec sum (pred, lst) =
  List.fold (fun acc x -> acc + (if pred(x) then x else 0)) 0 lst

let ex_sum = sum((fun x -> x > 2), [1; 2; 3; 4])

//hmm I wonder if there is a better way to do:
// if (pred(x)) then op(x, y) else y
// we seem to be doing it a fair bit

//5.4
let rec downto1 f n e =
  match n with
  | x when x <= 0 -> e
  | x -> downto1 f (n-1) (f(n, e))

let ex_downto1 = downto1 (fun (x,e) -> (x*2)::e) 6 []

let down_fun_list f n = downto1 (fun (x,e) -> f(x)::e) n []

let ex_down_fun_list = down_fun_list (fun (x) -> x * x) 6

//5.5 -- map colouring - with higher order functions
let rec isMember x = function
  | y::ys -> x = y || (isMember x ys)
  | [] -> false

type Country = string
type Map = (Country * Country) list
type Colour = Country list
type Colouring = Colour list

// decides whether two countries are neighbours
let areNb map c1 c2 =
  isMember (c1, c2) map || isMember (c2, c1) map

// decides whether a colour can be extended by a country
let canBeExtBy map lstOfCountriesForAColour cntry =
  List.forall
    (fun colouredCtry -> not(areNb map colouredCtry cntry))
    lstOfCountriesForAColour

// extends a colouring by an extra country
// - what we really want to do is find the first instance that
// matches a predicate and return a list replacing just that one.
// There's no built in function for that though. See this
// stack overflow question for more info
// http://stackoverflow.com/questions/11455367/is-replacing-a-list-element-an-anti-pattern
let extColouring map lstOfColoursAndTheirCountries cntry =
  let extendable = List.tryFind (fun x ->
    canBeExtBy map x cntry) lstOfColoursAndTheirCountries
  match extendable with
  | None -> lstOfColoursAndTheirCountries @ [[cntry]]
  | Some(lstOfCountriesForAColour) ->
    List.map (fun x ->
      if x = lstOfCountriesForAColour
      then cntry::lstOfCountriesForAColour
      else x)
      lstOfColoursAndTheirCountries

let addElem x ys = if isMember x ys then ys else x::ys

// computes a list of countries in a map
let countries map =
  List.foldBack (fun (c1, c2) acc -> addElem c1 (addElem c2 (acc))) map []

// builds a colouring for a list of countries
let rec buildColouring map cntryList =
  List.foldBack (fun x acc -> extColouring map acc x) cntryList []

let colourMap map = buildColouring map (countries map)

let ex_colourMap = colourMap [("a", "b"); ("c", "d"); ("d", "a")]

//5.6
//These exercises would be a lot easier using Maps, but that sort of defeats the
//purpose, so I've used Sets throughout
//5.6.1 domain of a relation between sets
let dom r =
  Set.map (fun x -> fst(x)) r

let ex_dom = dom (Set.ofList [("a", 1); ("b", 2); ("d", 4)])

//5.6.2 range of a relation between sets (co-domain)
let rng r =
  Set.map (fun x -> snd(x)) r

let ex_rng = rng (Set.ofList [("a", 1); ("b", 2); ("d", 4)])

//5.6.3 apply the relation r to element a
let apply a r =
  Set.fold (fun (acc:Set<'b>) x ->
    match x with
    | (a', b) when a' = a -> Set.add b acc
    | _ -> acc
    ) Set.empty r

let ex_apply = apply "b" (Set.ofList [("a", 1); ("b", 2); ("d", 4); ("b", 5)])

//5.6.4 - calculates the symetric subset of the relation r
// a relation is symetric if for evey (x, y) in r, (y, x) is also in r
let symetric r =
  Set.fold (fun (acc:Set<'a*'a>) x ->
    match x with
    | (a, b) when (Set.contains (b, a) r)  -> Set.add x acc
    | _ -> acc
  ) Set.empty r

let ex_symetric = symetric (Set.ofList [("a", "d"); ("b", "a"); ("d", "a"); ("b", "d")])

//5.6.5 - composition of two relations
// one way to look at this is flattening a middle level in a tree
// I'm not sure if we are supposed to use unionMany... It's pretty
// mechanical to replace with a fold and union
let compose r1 r2 =
  Set.unionMany (Set.map (fun a ->
    let bmapped = apply a r1
    let cmapped = Set.unionMany (Set.map (fun b -> apply b r2) bmapped)
    Set.map (fun c -> (a,c)) cmapped
  ) (dom r1))

let ex_compose =
  compose (Set.ofList [("a", 1); ("b", 2); ("d", 4); ("b", 5); ("e", 1); ("f", 6)])
    (Set.ofList [(1, true); (2, true); (3, true); (4, false); (5, false)])

//5.6.6 - transitive closure
let transitive r =
  List.fold (fun acc n ->
    //I feel like this could be done better with unfold
    let comp = (List.reduce (<<) (List.map (fun x -> compose r) [1..n]))
    Set.union acc (comp r)
  ) r [1..(Set.count r)-1]

let ex_transitive = transitive (Set.ofList [("a", "d"); ("b", "a"); ("d", "a"); ("b", "d")])
let ex_transitive2 = transitive (Set.ofList [("a", "b"); ("b", "c"); ("c", "d"); ("d", "e")])
let cnt = Set.count ex_transitive2

//5.7 creates a set of subsets of {1, 2, ..., n} of length k
// one version, using a cartesian product
let allsubsets n k =
  //this would be a lot easier if F# came with a cartesian product function for sets
  let superset = Set.map (fun x -> Set.singleton x) (Set.ofList [1..n])
  if k > n then failwith "k must be < or equal to n"
  elif k = 0 then Set.empty
  elif k = 1 then superset
  else
    //not a normal cartesian product function...
    // normal ones would have tuples, not sets in each element, but this makes
    // things easy later. We could have done it with tuples though
    let cartesian_set_product xs ys =
      Set.unionMany (Set.map (fun x -> (Set.map (fun y -> Set.union x y) ys)) xs)
    let superset_cartesian_set_product = cartesian_set_product superset

    let multi_cartesian = List.reduce (<<) (List.map (fun x -> superset_cartesian_set_product) [1..k-1])
    let result = multi_cartesian superset
    Set.filter (fun x -> k = (Set.count x)) result

let ex_allsubsets = allsubsets 4 3
let ex_allsubsets_2 = allsubsets 4 4
let ex_allsubsets_3 = allsubsets 5 2
let ex_allsubsets_4 = allsubsets 5 1
let ex_allsubsets_5 = allsubsets 5 0

//another way using lists:
let allsubsets_alt n k =
  if k > n then failwith "k must be < or equal to n"
  elif k = 0 then Set.ofList []
  else
  let superset = List.map (fun x -> List.singleton x) [1..n]
  let result =
    List.fold (fun acc _ ->
      List.collect (fun lst ->
        match lst with
          | y::_ when y < n ->
            List.map (fun i -> i::lst) [y+1..n]
          | _ -> []
        ) acc
     ) superset [1..k-1]
  Set.ofList (List.map (fun lst -> Set.ofList lst) result)

let ex_allsubsets_alt = allsubsets_alt 4 3
let ex_allsubsets_alt_2 = allsubsets_alt 4 4
let ex_allsubsets_alt_3 = allsubsets_alt 5 2
let ex_allsubsets_alt_4 = allsubsets_alt 5 1
let ex_allsubsets_alt_5 = allsubsets_alt 5 0

//5.8 makeBill3 using Map.fold rather than Map.foldBack
let makeBill3_58 register purchase =
  let f (infos, billPrice) articleCode noPieces =
    let (articleName, articlePrice) = Map.find articleCode register
    let linePrice = noPieces * articlePrice
    ((noPieces, articleName, linePrice)::infos, linePrice + billPrice)
  Map.fold f ([], 0) purchase

let ex_makeBill3_58 = makeBill3_58 reg1 purchaseMap

//5.9 declare function to give a purchase map on the basis of a list of items
// this definition is very vague... I think this is what they want?
let getPurchaseMap lst =
  List.fold (fun map item ->
    match (Map.tryFind item map) with
    | None -> Map.add item 1 map
    | Some(v) -> Map.add item (v+1) map) Map.empty lst
let ex_getPurchaseMap = getPurchaseMap ["a1"; "a3"; "a1"; "a1"; "a2"; "a3"]

//5.10 expand the cash register example to take discounts for certain articles
//into account
// the book is very vague on what a discount is...
// I'm just going to assume it's x off for every N you buy
type DiscountRegister = Map<ArticleCode, NoPieces * int>

let makeDiscountBill register purchase discountRegister =
  let f (infos, billPrice) articleCode noPieces =
    let (articleName, articlePrice) = Map.find articleCode register
    let discount = Map.tryFind articleCode discountRegister
    let linePrice =
      match discount with
      | None -> noPieces * articlePrice
      | Some(discountMatch, freeAmount) ->
        let sets = (noPieces / discountMatch)
        let freeTotal = sets * freeAmount
        (noPieces - freeTotal) * articlePrice
    ((noPieces, articleName, linePrice)::infos, linePrice + billPrice)
  Map.fold f ([], 0) purchase

let discount_purchase = Map.ofList [("a2", 3); ("a1", 11); ("a3", 3)]
let discounts = Map.ofList [("a1", (5, 1)); ("a3", (3, 2))]
let ex_makeDiscountBill = makeDiscountBill reg1 discount_purchase discounts


//5.11 do 4.23 (dating service) with Set and Map
type Sex = Male | Female
type file = { name: string; ph: string; sex: Sex ; yob: int; themes: string Set }

let client1 = { name = "Bob"; ph = "555";
  sex = Male; yob = 1992; themes = Set.ofList ["walks"; "sailing"]}
let client2 = { name = "Joe"; ph = "555";
  sex = Male; yob = 1987; themes = Set.ofList ["partying"; "football"]}
let client3 = { name = "Jenny"; ph = "555";
  sex = Female; yob = 1996; themes = Set.ofList ["partying"; "shopping"]}
let client4 = { name = "Stacy"; ph = "555";
  sex = Female; yob = 1998; themes = Set.ofList ["partying"; "sailing"]}
let client5 = { name = "Mary"; ph = "555";
  sex = Female; yob = 1981; themes = Set.ofList ["partying"; "walks"]}

let rec find_matches client allClients =
  let rec themes_match (clientA, clientB) =
    let intersect = Set.intersect clientA clientB
    not (Set.isEmpty intersect)

  let check_match (clientA, clientB) =
    clientA.sex <> clientB.sex //hetro only apparently
    && (abs(clientA.yob - clientB.yob) < 10)
    && themes_match(clientA.themes, clientB.themes)

  List.filter (fun x -> check_match(client, x)) allClients

let ex_find_matches_1 = find_matches client1 [client1; client2; client3; client4; client5]
let ex_find_matches_2 = find_matches client2 [client1; client2; client3; client4; client5]
let ex_find_matches_3 = find_matches client3 [client1; client2; client3; client4; client5]
let ex_find_matches_4 = find_matches client4 [client1; client2; client3; client4; client5]
let ex_find_matches_5 = find_matches client5 [client1; client2; client3; client4; client5]
