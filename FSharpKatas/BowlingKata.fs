﻿namespace BowlingKata

open NUnit.Framework

module Bowling =
    let rec splitIntoFramesWithBonuses rolls frame =
        seq {
            if frame <= 10 then
                match rolls with
                | hd::hd2::hd3::tl when hd + hd2 >= 10 ->
                    yield [hd; hd2; hd3]
                    if hd = 10 then
                        yield! splitIntoFramesWithBonuses (hd2::hd3::tl) (frame + 1)
                    else
                        yield! splitIntoFramesWithBonuses (hd3::tl) (frame + 1)
                | hd::hd2::tl ->
                    yield [hd; hd2]
                    yield! splitIntoFramesWithBonuses tl (frame + 1)
                | _ -> ignore []
        }

    let score rolls = 
        let framesWithBonuses = splitIntoFramesWithBonuses rolls 1
        for frame in framesWithBonuses do
            printfn "Frame x:"
            printfn "%s" (List.map (sprintf "%i") frame |> String.concat ", ")
        let frameScores = Seq.map (fun x -> List.sum x) framesWithBonuses
        Seq.fold (+) 0 frameScores

module BowlingTests =

    [<Test>]
    let ``should score 0 on gutter game``() =
        Assert.AreEqual(0, Bowling.score []) 

    [<Test>]
    let ``should score 20 with all 1s``() =
        let rolls = List.replicate 20 1
        Assert.AreEqual(20, Bowling.score rolls) 

    [<Test>]
    let ``should score spare and bonus``() =
        let rolls = List.append [2; 8; 5; 0] (List.replicate 16 0)
        Assert.AreEqual(20, Bowling.score rolls) 

    [<Test>]
    let ``should score strike and bonuses``() =
        let rolls = List.append [10; 4; 5; 0] (List.replicate 16 0)
        Assert.AreEqual(28, Bowling.score rolls) 

    [<Test>]
    let ``should score strike then spare``() =
        let rolls = List.append [10; 4; 6; 2; 4; 0;] (List.replicate 14 0)
        //strike scoring: 10 + 4 + 6 = 20
        //spare scoring: 4 + 6 + 2 = 12
        //frame 3 scoring: 2 + 4 = 6
        Assert.AreEqual(38, Bowling.score rolls) 

    [<Test>]
    let ``should score final bonuses``() =
        let rolls = List.append (List.replicate 16 0) [0; 10; 10; 4; 6;]
        //spare scoring: 10 + 10 = 20
        //strike scoring: 10 + 4 + 6 = 20
        Assert.AreEqual(40, Bowling.score rolls) 

    [<Test>]
    let ``should perfect game``() =
        let rolls = List.replicate 12 10
        Assert.AreEqual(300, Bowling.score rolls) 
