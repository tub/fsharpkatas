﻿namespace TennisKata

open NUnit.Framework

    
type Score =
    Love
    | Fifteen
    | Thirty
    | Fourty
    | Adv
    | Win

type Players =
    Server
    | Receiver

module Tennis =

    let scoreToString = function
        | Love -> "Love"
        | Fifteen -> "15"
        | Thirty -> "30"
        | Fourty -> "40"
        | Adv -> "Advantage"
        | Win -> "Win"

    let advanceScore = function
        | Love -> Fifteen
        | Fifteen -> Thirty
        | Thirty -> Fourty
        | Fourty -> Adv
        | Adv -> Win
        | Win -> Love //reset

    let updateWinnerScore (winner, loser) =
        match winner, loser with
            | Fourty, Adv -> Fourty, Fourty //back to duece after losing advantage
            | Fourty, Fourty -> Adv, Fourty //gain the advantage
            | Fourty, loser -> Win, loser //Player wins if they have fourty and the other one doesn't
            | _, _ -> advanceScore(winner), loser

    let outputScore (server, receiver) =
        match server, receiver with
            | Win, _ -> "Winner Server"
            | _, Win -> "Winner Receiver"
            | Fourty, Fourty -> "Deuce"
            | Adv, Fourty -> "Advantage Server"
            | Fourty, Adv -> "Advantage Receiver"
            | (server, receiver) when server = receiver -> scoreToString server + " All"
            | (server, receiver)  -> scoreToString server + " - " + scoreToString receiver

    let swap (server, receiver) = receiver, server

    let score server receiver scoringPlayer = 
        let updateScoreFn =
            match scoringPlayer with
                | Server -> updateWinnerScore
                | Receiver -> swap >> updateWinnerScore >> swap //swap positions before and after calling update winner score

        updateScoreFn (server, receiver)
            |> outputScore


module TennisTests = 

    [<Test>]
    let FifteenLoveTest() =
        Assert.AreEqual("15 - Love", Tennis.score Love Love Server) 

    [<Test>]
    let LoveThirtyTest() =
        Assert.AreEqual("Love - 30", Tennis.score Love Fifteen Receiver)

    [<Test>]
    let ThirtyAllTest() =
        Assert.AreEqual("30 All", Tennis.score Thirty Fifteen Receiver)

    [<Test>]
    let DeuceTest() =
        Assert.AreEqual("Deuce", Tennis.score Thirty Fourty Server)

    [<Test>]
    let Deuce2Test() =
        Assert.AreEqual("Deuce", Tennis.score Fourty Thirty Receiver)

    [<Test>]
    let AdvTest() =
        Assert.AreEqual("Advantage Server", Tennis.score Fourty Fourty Server)

    [<Test>]
    let Adv2Test() =
        Assert.AreEqual("Advantage Receiver", Tennis.score Fourty Fourty Receiver)

    [<Test>]
    let LoseAdvTest() =
        Assert.AreEqual("Deuce", Tennis.score Fourty Adv Server)

    [<Test>]
    let LoseAdv2Test() =
        Assert.AreEqual("Deuce", Tennis.score Adv Fourty Receiver)

    [<Test>]
    let WinAdvTest() =
        Assert.AreEqual("Winner Server", Tennis.score Adv Fourty Server)

    [<Test>]
    let WinAdv2Test() =
        Assert.AreEqual("Winner Receiver", Tennis.score Fourty Adv Receiver)

    [<Test>]
    let WinTest() =
        Assert.AreEqual("Winner Server", Tennis.score Fourty Thirty Server)

    [<Test>]
    let Win2Test() =
        Assert.AreEqual("Winner Receiver", Tennis.score Love Fourty Receiver)
