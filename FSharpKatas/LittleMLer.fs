﻿module LittleMLer

//Chapter 1

type num =
    | Zero
    | One_more_than of num

let num2 = One_more_than(One_more_than(Zero))

type 'a open_faced_sandwich =
    | Bread of 'a
    | Slice of 'a open_faced_sandwich

let true_bread = Bread(true)
let int_bread = Bread(1)
let num_slice = Bread(One_more_than(Zero))

let number_open_sandwich = Slice(Bread(10))

let int_sandwich_sandwich = Bread(Bread(10))
let num_sandwich_sandwich = Bread(Bread(One_more_than(Zero)))

//Chapter 2

type shish_kebab =
    | Skewer
    | Onion of shish_kebab
    | Lamb of shish_kebab
    | Tomato of shish_kebab

let only_skewer = Skewer
let onion_skewer = Onion(Skewer)
let multi_skewer = Onion(Lamb(Onion(Skewer)))

let rec only_onions = function
    | Skewer -> true
    | Onion(x) -> only_onions(x)
    | _ -> false

let r1 = only_onions(only_skewer)
let r2 = only_onions(onion_skewer)
let r3 = only_onions(multi_skewer)

let rec is_vegetarian = function
    | Skewer -> true
    | Onion(x) -> is_vegetarian(x)
    | Tomato(x) -> is_vegetarian(x)
    | _ -> false

let is_v = is_vegetarian(Tomato(Onion(Tomato(Skewer))))

type 'a shish =
    | Bottom of 'a 
    | Onion of 'a shish
    | Lamb of 'a shish
    | Tomato of 'a shish

type rod =
    | Dagger
    | Fork
    | Sword

type plate =
    | Gold_plate
    | Silver_plate
    | Brass_plate

let rec is_vegie = function
   | Bottom(x) -> true
   | Onion(x) -> is_vegie(x)
   | Tomato(x) -> is_vegie(x)
   | _ -> false

let veg1 = is_vegie(Onion(Tomato(Bottom(Dagger))))
let veg2 = is_vegie(Tomato(Tomato(Bottom(Brass_plate))))

let rec what_bottom = function
    | Bottom(x) -> x
    | Onion(x) -> what_bottom(x)
    | Lamb(x) -> what_bottom(x)
    | Tomato(x) -> what_bottom(x)

let bottom1 = what_bottom(Onion(Tomato(Bottom(Dagger))))
let bottom2 = what_bottom(Tomato(Tomato(Bottom(52))))

//Chapter 3

//type pizza =
//    | Crust
//    | Cheese of pizza
//    | Onion of pizza
//    | Anchovy of pizza
//    | Sausage of pizza
//
//let rec remove_anchovy = function
//    | Crust -> Crust
//    | Cheese(x) -> Cheese(remove_anchovy(x))
//    | Onion(x) -> Onion(remove_anchovy(x))
//    | Anchovy(x) -> remove_anchovy(x)
//    | Sausage(x) -> Sausage(remove_anchovy(x))
//
//let no_anchovy = remove_anchovy(Anchovy(Onion(Anchovy(Anchovy(Cheese(Crust))))))
//
//let rec top_anchovy_with_cheese = function
//    | Crust -> Crust
//    | Cheese(x) -> Cheese(top_anchovy_with_cheese(x))
//    | Onion(x) -> Onion(top_anchovy_with_cheese(x))
//    | Anchovy(x) -> Cheese(Anchovy(top_anchovy_with_cheese(x)))
//    | Sausage(x) -> Sausage(top_anchovy_with_cheese(x))
//
//let top_anchovy = top_anchovy_with_cheese(Onion(Anchovy(Cheese(Anchovy(Crust)))))
//let top_no_anchovy = top_anchovy_with_cheese(Onion(Cheese(Sausage(Crust))))
//
//let replace1 = remove_anchovy(top_anchovy_with_cheese(Onion(Anchovy(Cheese(Anchovy(Crust))))))
//
//let rec subst_anchovy_by_chesse = function
//    | Crust -> Crust
//    | Cheese(x) -> Cheese(subst_anchovy_by_chesse(x))
//    | Onion(x) -> Onion(subst_anchovy_by_chesse(x))
//    | Anchovy(x) -> Cheese(subst_anchovy_by_chesse(x))
//    | Sausage(x) -> Sausage(subst_anchovy_by_chesse(x))
//
//let replace2 = subst_anchovy_by_chesse(Onion(Anchovy(Cheese(Anchovy(Crust)))))

//Chapter 4

type meza =
    | Shrimp
    | Calamari
    | Escargots
    | Hummus

type main =
    | Steak
    | Ravioli
    | Chicken
    | Eggplant

type salad = 
    | Green
    | Cucumber
    | Greek

type dessert =
    | Sundae
    | Mousse
    | Torte

let add_a_steak = function
    | (meza:meza) -> (meza, Steak)

let c4_mini_meal = add_a_steak(Escargots)

let eq_main = function
    | (main1:main, main2:main) -> main1 = main2

let c4_main_eq1 = eq_main(Steak, Steak)
let c4_main_eq2 = eq_main(Chicken, Steak)

let has_steak = function
    | (_:meza, main, _:dessert) -> main = Steak

let c4_has_steak1 = has_steak (Hummus, Ravioli, Sundae)
let c4_has_steak2 = has_steak (Shrimp, Steak, Mousse)

//Chapter 5

type 'a pizza =
    | Bottom
    | Topping of ('a * ('a pizza))

type fish =
    | Anchovy
    | Lox
    | Tuna

let c5_fish_pizza =
    Topping(Lox,
        Topping(Anchovy,
            Topping(Tuna,
                Topping(Anchovy,
                    Bottom))))

let rec rem_anchovy = function
    | Bottom -> Bottom
    | Topping(Anchovy, p) -> rem_anchovy(p)
    | Topping(x, p) -> Topping(x, rem_anchovy(p))

let c5_fish_pizza_no_anchovy = rem_anchovy(c5_fish_pizza)

let rec rem_fish_when = function
    | (fsh, Bottom) -> Bottom
    | (fsh, Topping(fsh2, p)) when fsh = fsh2 -> rem_fish_when(fsh, p)
    | (fsh, Topping(t, p)) -> Topping(t, rem_fish_when(fsh, p))

let rec rem_fish = function
    | (fsh, Bottom) -> Bottom
    | (fsh, Topping(t, p)) ->
        if fsh = t then
            rem_fish(fsh, p)
        else
            Topping(t, rem_fish(fsh, p))

let c5_fish_pizza_no_anchovy2 = rem_fish(Anchovy, c5_fish_pizza)
let c5_fish_pizza_no_tuna = rem_fish(Tuna, c5_fish_pizza)

let rec eq_fish = function
    | (f1, f2) -> f1 = f2

let anchovy_eq_anchovy = eq_fish (Anchovy, Anchovy)
let anchovy_eq_tuna = eq_fish (Anchovy, Tuna)

let rec rem_int = function
    | (i, Bottom) -> Bottom
    | (i, Topping(x, p)) when x = i -> rem_int(i, p)
    | (i, Topping(x, p)) -> Topping(x, rem_int(i, p))

let c5_rem_num = rem_int (3, Topping(3, Topping(2, Topping(3, Topping(1, Bottom)))))

let rec subst_fish = function
    | (s, r, Bottom) -> Bottom
    | (s, r, Topping(x, p)) when r = x -> Topping(s, subst_fish(s, r, p))
    | (s, r, Topping(x, p)) -> Topping(x, subst_fish(s, r, p))

let c5_sbst_fish_1 = subst_fish (Lox, Anchovy, c5_fish_pizza)
let c5_sbst_fish_2 = subst_fish (5, 3, Topping(3, Topping(2, Topping(3, Topping(1, Bottom)))))

let rec eq_num_long = function
    | (Zero, Zero) -> true
    | (Zero, One_more_than(x)) -> false
    | (One_more_than(x), Zero) -> false
    | (One_more_than(x), One_more_than(y)) -> eq_num_long(x, y)

let rec eq_num = function
    | (Zero, Zero) -> true
    | (One_more_than(x), One_more_than(y)) -> eq_num(x, y)
    | (x, y) -> false

let c5_eq_num_1 = eq_num (One_more_than(One_more_than(Zero)), One_more_than(One_more_than(Zero)))
let c5_eq_num_2 = eq_num (One_more_than(Zero), One_more_than(One_more_than(Zero)))

//Chapter 6

type fruit =
    | Peach
    | Apple
    | Pear
    | Lemon
    | Fig

type tree =
    | Bud
    | Flat of fruit * tree
    | Split of tree * tree

let rec flat_only = function
    | Bud -> true
    | Flat(f, t) -> flat_only(t)
    | Split(t1, t2) -> false

let c6_flat_only_1 = flat_only (Flat(Apple, Flat(Peach, Bud)))
let c6_flat_only_2 = flat_only (Split(Bud, Flat(Fig, Split(Bud, Bud))))

let rec split_only_book = function
    | Bud -> true
    | Flat(f, t) -> false
    | Split(t1, t2) ->
        if split_only_book(t1)
        then split_only_book(t2)
        else false

let rec split_only = function
    | Bud -> true
    | Flat(f, t) -> false
    | Split(t1, t2) -> split_only(t1) && split_only(t2)

let c6_split_only_1 = split_only (Split(Bud, Flat(Fig, Split(Bud, Bud))))
let c6_split_only_2 = split_only (Split(Bud, Split(Bud, Bud)))
let c6_split_only_5 = split_only (Split(Split(Split(Bud, Bud), Split(Bud, Bud)), Split(Bud, Bud)))

let rec contains_fruit_long = function
    | Bud -> false
    | Flat(f, t) -> true //Flat must have fruit by definition
    | Split(t1, t2) -> contains_fruit_long(t1) || contains_fruit_long(t2)

let rec contains_fruit (x) =
    not(split_only(x))

let c6_contains_fruit_1 = contains_fruit (Split(Bud, Flat(Fig, Split(Bud, Bud))))
let c6_contains_fruit_2 = contains_fruit (Split(Bud, Split(Bud, Bud)))

let rec larger_of(x, y) = if x < y then y else x

let rec height = function
    | Bud -> 0
    | Flat(f, t) -> 1 + height(t)
    | Split(t1, t2) -> 1 + larger_of(height(t1), height(t2))

let c6_height_1 = height (Split(Bud, Bud))
let c6_height_2 = height (Flat(Pear, (Flat(Peach, Bud))))
let c6_height_3 = height (Split(Split(Bud, Bud), Flat(Fig, Flat(Lemon, Flat(Apple, Bud)))))

let rec eq_fruit = function
    | (x:fruit, y:fruit) -> x = y

let rec subst_in_tree = function
    | (s, r, Bud) -> Bud
    | (s, r, Flat(x, t)) ->
        if eq_fruit (r, x)
        then (Flat(s, subst_in_tree(s, r, t)))
        else (Flat(x, subst_in_tree(s, r, t)))
    | (s, r, Split(t1, t2)) -> Split(subst_in_tree(s, r, t1), subst_in_tree(s, r, t2))

let c6_subst_in_tree_1 = subst_in_tree(Apple, Fig, Split(Split(Flat(Fig, Bud), Flat(Fig, Bud)), Flat(Fig, Flat(Lemon, Flat(Apple, Bud)))))

let rec occurs = function
    | (f, Bud) -> 0
    | (f, Flat(x, t)) ->
        if eq_fruit (f, x)
        then 1 + occurs(f, t)
        else occurs(f, t)
    | (f, Split(t1, t2)) -> occurs(f, t1) + occurs(f, t2)

let c6_occurs_1 = occurs(Fig, Split(Split(Flat(Fig, Bud), Flat(Fig, Bud)), Flat(Fig, Flat(Lemon, Flat(Apple, Bud)))))

type 'a slist =
    | Empty
    | Scons of 'a sexp * 'a slist
and
    'a sexp =
    | An_atom of 'a
    | A_slist of 'a slist

let rec occurs_in_slist = function
    | (o, Empty) -> 0
    | (o, Scons(x, y)) -> occurs_in_sexp(o, x) + occurs_in_slist(o, y)
and
    occurs_in_sexp = function
    | (o, An_atom(x)) -> if o = x then 1 else 0
    | (o, A_slist(x)) -> occurs_in_slist(o, x)

let c6_occurs_in_1 = occurs_in_slist(Fig,
    Scons(
        A_slist(
            Scons(An_atom(Fig), Scons(An_atom(Peach), Empty))),
                Scons(An_atom(Fig), Scons(An_atom(Lemon), Empty))))

let c6_occurs_in_2 = occurs_in_sexp(Fig, A_slist(Scons(An_atom(Fig), Scons(An_atom(Peach), Empty))))

let rec subst_in_slist = function
    | (s, r, Empty) -> Empty
    | (s, r, Scons(exp, lst)) -> Scons(subst_in_sexp(s, r, exp), subst_in_slist(s, r, lst))
and
    subst_in_sexp = function
    | (s, r, An_atom(x)) -> if r = x then An_atom(s) else An_atom(x)
    | (s, r, A_slist(x)) -> A_slist(subst_in_slist(s, r, x))

let c6_subst_in_1 = subst_in_slist(Pear, Fig,
    Scons(
        A_slist(
            Scons(An_atom(Fig), Scons(An_atom(Peach), Empty))),
                Scons(An_atom(Fig), Scons(An_atom(Lemon), Empty))))

let c6_subst_in_2 = subst_in_sexp(Pear, Fig, A_slist(Scons(An_atom(Fig), Scons(An_atom(Peach), Empty))))

let eq_fruit_in_atom = function
    | (f, An_atom(x)) -> eq_fruit(f, x)
    | (f, A_slist(x)) -> false

let slist_is_empty = function
    | Empty -> true
    | Scons(exp, lst) -> false

//my version is a bit different from the books version.
//It runs a lot better when the head of a cons is an empty list after removal
let rec rem_in_slist_mine = function
    | (r, Empty) -> Empty
    | (r, Scons(An_atom(exp), lst)) ->
        if eq_fruit(r, exp)
        then rem_in_slist_mine(r, lst)
        else Scons(An_atom(exp), rem_in_slist_mine(r, lst))
    | (r, Scons(A_slist(exp), lst)) ->
        if slist_is_empty(rem_in_slist_mine(r, exp))
        then rem_in_slist_mine(r, lst)
        else Scons(A_slist(rem_in_slist_mine(r, exp)), rem_in_slist_mine(r, lst))

let rec rem_in_slist = function
    | (r, Empty) -> Empty
    | (r, Scons(An_atom(exp), lst)) ->
        if eq_fruit(r, exp)
        then rem_in_slist(r, lst)
        else Scons(An_atom(exp), rem_in_slist_mine(r, lst))
    | (r, Scons(A_slist(exp), lst)) ->
        Scons(A_slist(rem_in_slist(r, exp)), rem_in_slist(r, lst))

let c6_rem_in_1 = rem_in_slist(Fig, Scons(An_atom(Fig), Scons(An_atom(Peach), Empty)))

//This has a nicer result when run with rem_in_slist_mine
let c6_rem_in_2 = rem_in_slist_mine(Fig,
    Scons(
        A_slist(Scons(An_atom(Fig), Empty)),
        Scons(An_atom(Fig), Scons(An_atom(Lemon), Empty))))

//Chapter 7

let eq_int(x, y) = x = y

let true_maker(x) =
    true

type bool_or_int =
    | Hot of bool
    | Cold of int

let hot_maker(x) =
    Hot

let half(f) =
    Hot(
        true_maker(
            if true_maker(5)
            then f
            else true_maker))

let c7_half = half (fun () -> false)

type chain =
    | Link of (int * (int -> chain))

let rec ints(n) =
    Link(n + 1, ints)

let rec skips(n) =
    Link(n + 2, skips)

let divides_evenly(n, c) =
    eq_int((n % c), 0)

let is_mod_5_or_7(n) =
    if divides_evenly(n, 5)
    then true
    else divides_evenly(n, 7)

let rec some_ints(n) =
    if is_mod_5_or_7(n + 1)
    then Link(n + 1, some_ints)
    else some_ints(n + 1)

let c7_some_ints_0 = some_ints(0)
let c7_some_ints_5 = some_ints(5)
let c7_some_ints_7 = some_ints(7)

let rec chain_item = function
    | (n, Link(i, f)) ->
        if eq_int(n, 1)
        then i
        else chain_item(n - 1, f(i))

let c7_chain_item_1 = chain_item (3, some_ints(0))
let c7_chain_item_2 = chain_item (10, some_ints(0))
let c7_chain_item_3 = chain_item (50, skips(0))

let rec is_prime(n) =
    has_no_divisors(n, n - 1)
and
    has_no_divisors(n, c) =
       if c = 1
       then true
       elif divides_evenly(n, c)
       then false
       else has_no_divisors(n, c - 1)

let rec primes(n) =
    if is_prime(n + 1)
    then Link(n + 1, primes)
    else primes(n + 1)

let c7_chain_item_primes_1 = chain_item (12, primes(1))

let rec fibs n m =
    Link(n + m, fibs(m))

//Chapter 8

type 'a list =
    | Empty
    | Cons of 'a * 'a list

type orapl =
    | Orange
    | Apple

let eq_orapl = function
    | (Orange, Orange) -> true
    | (Apple, Apple) -> true
    | (_, _) -> false

let rec subst_orapl = function
    | (s, m, Empty) -> Empty
    | (s, m, Cons(x, lst)) ->
        if eq_orapl(m, x)
        then Cons(s, subst_orapl(s, m, lst))
        else Cons(x, subst_orapl(s, m, lst))

let rec subst = function
    | (rel, s, m, Empty) -> Empty
    | (rel, s, m, Cons(x, lst)) ->
        if rel(m, x)
        then Cons(s, subst(rel, s, m, lst))
        else Cons(x, subst(rel, s, m, lst))

let c8_int_list = Cons(15, Cons(6, Cons(15, Cons(17, Cons(15, Cons(8, Empty))))))

let c8_subst_1 = subst(eq_int, 11, 15, c8_int_list)

let less_than(x, y) = x < y

let c8_subst_2 = subst(less_than, 11, 15, c8_int_list)

let in_range((x, y), z) = less_than(x, z) && less_than(z, y)

let c8_subst_3 = subst(in_range, 22, (11, 16), c8_int_list)

let rec subst_pred = function
    | (pred, s, Empty) -> Empty
    | (pred, s, Cons(x, lst)) ->
        if pred(x)
        then Cons(s, subst_pred(pred, s, lst))
        else Cons(x, subst_pred(pred, s, lst))

let is_15(x) = eq_int(15, x)

let c8_subst_pred_1 = subst_pred(is_15, 22, c8_int_list)

let less_than_15(x) = less_than(x, 15)

let c8_subst_pred_2 = subst_pred(less_than_15, 11, c8_int_list)

let in_range_11_16(x) = in_range((11, 16), x)

let c8_subst_pred_3 = subst_pred(in_range_11_16, 22, c8_int_list)

let in_range_c (small, large) (x) = in_range((small, large), x)

let c8_subst_pred_4 = subst_pred(in_range_c(11, 16), 22, c8_int_list)

let rec subst_c pred = function
    | (n, Empty) -> Empty
    | (n, Cons(x, lst)) ->
        if pred(x)
        then Cons(n, subst_c(pred)(n, lst))
        else Cons(x, subst_c(pred)(n, lst))

let c8_subst_c_4 = subst_c (in_range_c(11, 16)) (22, c8_int_list)

let subst_c_in_range_11_16 = subst_c (in_range_c(11, 16))

let rec combine_orig = function
    | (Empty, Empty) -> Empty
    | (Empty, Cons(b, l2)) -> Cons(b, l2)
    | (Cons(a, l1), Empty) -> Cons(a, l1)
    | (Cons(a, l1), Cons(b, l2)) -> Cons(a, combine_orig(l1, Cons(b, l2)))

//simplfied version of above
let rec combine = function
    | (Empty, l2) -> l2
    | (Cons(a, l1), l2) -> Cons(a, combine(l1, l2))

let c8_combine_1 = combine(
    Cons(1, Cons(2, Cons(3, Empty))),
    Cons(5, Cons(4, Cons(7, Cons(9, Empty)))))

let rec combine_c = function
    | Empty -> (fun lst -> lst)
    | Cons(a, l1) -> (fun l2 -> Cons(a, (combine_c l1 l2)))

let c8_combine_c_1 = combine_c (Cons(1, Cons(2, Cons(3, Empty)))) (Cons(5, Cons(4, Cons(7, Cons(9, Empty)))))

let rec prefixer_123 = function
    | lst -> Cons(1, Cons(2, Cons(3, lst)))

let bse lst = lst

let rec combine_s = function
    | Empty -> bse
    | Cons(a, l1) -> make_cons(a, combine_s(l1))
and
    make_cons (a, fn) l2 = Cons(a, fn l2)

let c8_combine_s_1 = combine_s (Cons(1, Cons(2, Cons(3, Empty))))

//Chapter 9

type box =
    | Bacon
    | Ix of int

let is_bacon = function
  | Bacon -> true
  | Ix(x) -> false

let rec where_is_helper i = function
  | Empty -> 0
  | Cons(Bacon, lst) -> i
  | Cons(Ix(x), lst) ->
    where_is_helper (i + 1) lst

let rec where_is_mine = function
  | lst -> where_is_helper 1 lst

exception No_bacon of int

let rec where_is = function
  | Empty -> raise (No_bacon 0)
  | Cons(Bacon, lst) -> 1
  | Cons(Ix(x), lst) -> 1 + where_is(lst)

let c9_where_is_1 =
  where_is(
    Cons(Ix(5),
      Cons(Ix(13),
        Cons(Bacon,
          Cons(Ix(8),
            Empty)))))

let c9_where_is_2 =
  try
    where_is(
      Cons(Ix(5),
        Cons(Ix(13),
          Cons(Ix(8),
            Empty))))
  with No_bacon(x) -> x

exception Out_of_range

// I solved it a bit differently
let rec check_mine = function
  | (f, n, Empty) -> raise Out_of_range
  | (f, n, Cons(Bacon, lst)) -> n
  | (f, n, Cons(Ix(x), lst)) -> check_mine(f, x, f(x))

let rec list_stop_mine = function
  | Empty -> fun n -> Empty
  | Cons(x, lst) -> function
    | 1 -> Cons(x, lst)
    | n -> list_stop_mine (lst) (n - 1)

let rec find_mine = function
  | (n, Empty) -> raise Out_of_range
  | (n, lst) -> check_mine(list_stop_mine lst, n, lst)


//the book version
let rec list_item = function
  | (n, Empty) -> raise Out_of_range
  | (n, Cons(x, lst)) ->
    if n = 1
    then x
    else list_item(n - 1, lst)

let rec find = function
  | (n, lst) ->
  try
    check(n, lst, list_item(n, lst))
  with Out_of_range -> find(n/2, lst)
and
    check = function
    | (n, lst, Bacon) -> n
    | (n, lst, Ix(i)) -> find(i, lst)


let c9_find_1 =
  find(1,
    Cons(Ix(2),
      Cons(Bacon,
        Empty)))

let c9_find_2 =
  find(1,
    Cons(Ix(3),
      Cons(Bacon,
        Cons(Ix(2),
          Empty))))

let c9_find_3 =
  find(1,
    Cons(Ix(5),
      Cons(Ix(4),
        Cons(Bacon,
          Cons(Ix(3),
            Cons(Ix(2),
              Empty))))))

let c9_find_4 =
  find(1,
    Cons(Ix(3),
      Cons(Bacon,
        Cons(Ix(4),
          Empty))))

let rec path = function
  | (n, lst) ->
  try
    Cons(n, check_path(lst, list_item(n, lst)))
  with Out_of_range -> path(n/2, lst)
and
    check_path = function
    | (lst, Bacon) -> Empty
    | (lst, Ix(i)) -> path(i, lst)


let c9_path_1 =
  path(1,
    Cons(Ix(2),
      Cons(Bacon,
        Empty)))

let c9_path_2 =
  path(1,
    Cons(Ix(3),
      Cons(Bacon,
        Cons(Ix(2),
          Empty))))

let c9_path_3 =
  path(1,
    Cons(Ix(5),
      Cons(Ix(4),
        Cons(Bacon,
          Cons(Ix(3),
            Cons(Ix(2),
              Empty))))))

let c9_path_4 =
  path(1,
    Cons(Ix(3),
      Cons(Bacon,
        Cons(Ix(4),
          Empty))))

//Chapter 10

let rec plus_mine = function
  | (x, 0) -> x
  | (x, y) -> 1 + plus_mine(x, y - 1)

let rec is_zero = function
  | 0 -> true
  | x -> false

exception Too_small

let pred(n) = 
  if is_zero(n)
  then raise Too_small
  else n - 1

let succ(n) = n + 1

let rec plus(n, m) =
  if (is_zero(n))
  then m
  else succ(plus(pred(n), m))
  

let c10_plus_1 = plus(1, 0)
let c10_plus_2 = plus(1, 1)
let c10_plus_3 = plus(2, 1)
let c10_plus_4 = plus(0, 6)


let is_zero_num = function
  | Zero -> true
  | n -> false

let pred_num = function
  | n when is_zero_num(n) -> raise Too_small
  | One_more_than(n) -> n

let succ_num(n) = One_more_than(n)
  
let rec plus_num = function
  | (n, m) when is_zero_num(n) -> m
  | (n, m) -> succ_num(plus_num(pred_num(n), m))

let c10_plus_num_1 = plus_num(One_more_than(Zero), Zero)
let c10_plus_num_2 = plus_num(One_more_than(Zero), One_more_than(Zero))
let c10_plus_num_3 = plus_num(One_more_than(One_more_than(Zero)), One_more_than(Zero))
let c10_plus_num_4 = plus_num(Zero, One_more_than(One_more_than(Zero)))

    
// We can go no further with F# ... it's module sytem is not extensive enough to support functors
//Continuing in Ocaml:

(*

module type N =
  sig
    type number
    exception Too_small
    val succ : number -> number
    val pred : number -> number
    val is_zero : number -> bool
  end

module NumberAsNum : N =
  struct
    type num = Zero | One_more_than of num
    type number = num
    exception Too_small
    let succ(n) = One_more_than(n)
    let pred = function
      | Zero -> raise Too_small
      | One_more_than(n) -> n
    let is_zero = function
      | Zero -> true
      | _ -> false
  end
 
module NumberAsInt : N =
 struct
  type number = int
  exception Too_small
  let is_zero = function
   | 0 -> true
   | x -> false
  let pred(n) = 
    if is_zero(n)
    then raise Too_small
    else n - 1
  let succ(n) = n + 1
 end

module type P =
  sig
    type number
    val plus : (number * number) -> number
  end
 
module Pon(A_N : N) : P =
 struct
  type number = A_N.number
  let rec plus(n, m) =
    if A_N.is_zero(n)
    then m
    else A_N.succ(plus(A_N.pred(n), m))
 end

*)
