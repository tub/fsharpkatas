﻿namespace RomanToDecimalKata

open NUnit.Framework

module RomanToDecimal =

    let MapToDecimal numeral =
        match numeral with
        | 'I' -> 1
        | 'V' -> 5
        | 'X' -> 10
        | 'L' -> 50
        | 'C' -> 100
        | 'D' -> 500
        | 'M' -> 1000
        | _ -> 0
    
    let convert (roman:string) =
        roman.ToCharArray()
            |> Array.map MapToDecimal 
            |> Array.rev
            |> Array.fold (fun (value, prev) curr -> 
                if prev > curr then (value - curr, curr)
                else (value + curr, curr)
                ) (0, 0)
            |> fst

module RomanToDecimalTests = 

    [<Test>]
    let ``should convert I``() =
        Assert.AreEqual(1, RomanToDecimal.convert "I") 

    [<Test>]
    let ``should convert II``() =
        Assert.AreEqual(2, RomanToDecimal.convert "II") 

    [<Test>]
    let ``should convert IV``() =
        Assert.AreEqual(4, RomanToDecimal.convert "IV") 

    [<Test>]
    let ``should convert V``() =
        Assert.AreEqual(5, RomanToDecimal.convert "V") 

    [<Test>]
    let ``should convert MMLXVII``() =
        Assert.AreEqual(2068, RomanToDecimal.convert "MMLXVIII") 

    [<Test>]
    let ``should convert MCMXCIV``() =
        Assert.AreEqual(1994, RomanToDecimal.convert "MCMXCIV") 
