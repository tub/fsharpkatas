﻿namespace DecimalToRomanKata

open NUnit.Framework

module DecimalToRoman =

    let getOneValueForPos i =
        match i with
        | 0 -> "I" //
        | 1 -> "X" // 10s
        | 2 -> "C" // 100s
        | 3 -> "M" // 1000s
        | _ -> ""

    let getFiveValueForPos i =
        match i with
        | 0 -> "V" // 5
        | 1 -> "L" // 50
        | 2 -> "C" // 500
        | _ -> ""
    
    //Roman numerals work in 5s and 10s, 4s and 9s are special cases
    //If we convert the decimal into a string, we should be able to go
    //through each position and work out what we need
    let convert decimal =
        decimal.ToString().ToCharArray()
            |> Array.map (System.Char.GetNumericValue >> int)
            |> Array.rev
            |> Array.mapi (fun i v ->
                match v with
                | 9 -> (getOneValueForPos i) + (getOneValueForPos (i+1))
                | v when v >= 5 -> (getFiveValueForPos i) + (String.replicate (v - 5) (getOneValueForPos i))
                | 4 -> (getOneValueForPos i) + (getFiveValueForPos i)
                | _ -> (String.replicate v (getOneValueForPos i)))
            |> Array.rev
            |> String.concat "" 

module DecimalToRomanTests = 

    [<Test>]
    let ``should convert 1``() =
        Assert.AreEqual("I", DecimalToRoman.convert 1) 

    [<Test>]
    let ``should convert 2``() =
        Assert.AreEqual("II", DecimalToRoman.convert 2) 

    [<Test>]
    let ``should convert 4``() =
        Assert.AreEqual("IV", DecimalToRoman.convert 4) 

    [<Test>]
    let ``should convert 5``() =
        Assert.AreEqual("V", DecimalToRoman.convert 5) 

    [<Test>]
    let ``should convert 2068``() =
        Assert.AreEqual("MMLXVIII", DecimalToRoman.convert 2068) 

    [<Test>]
    let ``should convert 1994``() =
        Assert.AreEqual("MCMXCIV", DecimalToRoman.convert 1994) 
