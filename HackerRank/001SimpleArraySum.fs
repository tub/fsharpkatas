﻿module SimpleArraySum

let impl count elements =
  elements.ToString().Split()
  |> Array.take count
  |> Array.map int
  |> Array.sum

let result = impl 6 "1 2 3 4 10 11"

////Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
//[<EntryPoint>]
//let main argv = 
//    let count = Console.ReadLine() |> int
//    let elements = Console.ReadLine()
//    
//    let result =
//      elements.Split()
//      |> Array.map int
//      |> Array.take count
//      |> Array.sum
//    printfn "%d" result
//  
//    0 // return an integer exit code