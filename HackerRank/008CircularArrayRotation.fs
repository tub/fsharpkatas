﻿module CircularArrayRotation
open System

let impl (n:int) (k':int) (arr:int []) queries = 
  let k = k' % n
  let rotated = [|0..n-1|] |> Array.map (fun i -> arr.[(i + n - k) % n])
  queries |> Array.map (fun q -> rotated.[q])

let result = impl 4 6 [| 1; 2; 3; 4|] [| 0; 1; 2; 3;|]

//Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
////read the values from STDIN
//let [|n; k'; q|] = Console.ReadLine().Split() |> Array.map int
//let k = k' % n
//let arr = Console.ReadLine().Split() |> Array.map int |> Array.take n
//let queries = Array.init q (fun _ -> Console.ReadLine() |> int)
//
//let rotated = [|0..n-1|] |> Array.map (fun i -> arr.[(i + n - k) % n])
//let results = queries |> Array.map (fun q -> rotated.[q])
//
//printfn "%s" (String.Join("\n", results))
//
//0 // return an integer exit code