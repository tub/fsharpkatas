﻿module TheCoinChangeProblem
open System
open System.Text
open System.Collections.Generic 

//didn't actually use this, but could be very useful
let memoize f =
  let mem = Dictionary<'a, 'b>(); //need practice to get more familiarity with how 'a, 'b are working
  let rec g key = h g key
  and h r key =
    match mem.TryGetValue(key) with
    | (true, value) -> value
    | _ ->
      let value = f g key
      mem.Add(key, value)
      value
  g

//recursive implementation
let rec implrec total coins (mem:Dictionary<int * int, int64>) =
  if Array.length coins = 0 then
    0L
  else
    let largestCoin = coins.[0]
    let key = (total, largestCoin)
    match mem.TryGetValue(key) with
      | (true, value) -> value
      | _ ->
        let value =
          coins |> Array.map (fun c -> 
            match total - c with
            | 0 -> 1L
            | n when n < 0 -> 0L
            | n -> implrec n (Array.filter (fun y -> y <= c) coins) mem) 
          |> Array.sum
        mem.Add(key, value)
        value

let impl total coins = 
    let mem = Dictionary<int * int, int64>()
    let orderedcoins = Array.sortDescending coins
    implrec total orderedcoins mem

let resultrec1 = impl 4 [|1; 2; 3|]
let resultrec2 = impl 10 [|2; 5; 3; 6|]
let resultrec3 = impl 100 [|2; 5; 3; 6|]
let resultrec4 = impl 150 [|1..50|]
let resultrec5 = impl 10 [|15; 11|]
let resultrec6 = impl 0 [|15; 11|]
let resultrec7 = impl 100 [|1; 5; 10; 25; 50; 100|]
let resultrec8 = impl 101 [|2; 4; 10; 50; 100|]
let resultrec9 = impl 100 [|1..100|]

//dynamic programming implementation (wrong)
let impldyn total coins = 
  if List.length coins = 0 then
    0
  else
    let dict = Collections.Generic.Dictionary<int, int>();
    for c in coins do //setup coins themselves to be 1 in change
      dict.Add(c, 1)
    for n in [1..List.max coins] do
      if not (dict.ContainsKey n) then
        dict.Add(n, 0) //setup defaults for things we know we can't make chage from
    for n in [1..total] do
      if not (dict.ContainsKey n) then
        let v = List.map (fun c -> dict.[n - c]) coins |> List.sum
        dict.Add(n, v + 1)
    dict.[total]

let resultdyn1 = impldyn 4 [1; 2; 3]
let resultdyn2 = impldyn 10 [2; 5; 3; 6]

//Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//open System.Text
//open System.Collections.Generic 
//
//let memoize f =
//  let mem = Dictionary<'a, 'b>();
//  let rec g key = h g key
//  and h r key =
//    match mem.TryGetValue(key) with
//    | (true, value) -> value
//    | _ ->
//      let value = f g key
//      mem.Add(key, value)
//      value
//  g
//
////recursive implementation
//let rec implrec f (total, coins) =
//  if Array.length coins = 0 then
//    0L
//  else
//    coins
//      |> Array.map (fun c -> 
//        match total - c with
//        | 0L -> 1L
//        | n when n < 0L -> 0L
//        | n -> f (n, (Array.filter (fun y -> y <= c) coins)))
//      |> Array.sum
//
//let impl total coins = 
//    let orderedcoins = Array.sortDescending coins
//    (memoize implrec) (total, orderedcoins)
//      
//let [|total; n|] = Console.ReadLine().Split() |> Array.map int64
//let coins = Console.ReadLine().Split() |> Array.map int64 |> Array.take (int n)
//let result = impl total coins
//
//printfn "%d" result
//
//0 // return an integer exit code