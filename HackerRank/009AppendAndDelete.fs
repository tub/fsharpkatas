﻿module AppendAndDelete
open System

let rec equalLen (s1:char list) (s2:char list) = 
  match (s1, s2) with
    | ([], _) -> 0
    | (_, []) -> 0
    | (s1h::xs1, s2h::xs2) when s1h = s2h -> equalLen xs1 xs2 + 1
    | (s1h::xs1, s2h::xs2) -> 0

let impl (s1:string) (s2:string) n =
  if (n >= s1.Length + s2.Length) then
    true
  else
    let eq = equalLen (List.ofArray(s1.ToCharArray())) (List.ofArray(s2.ToCharArray()))
    let d1 = s1.Length - eq
    let d2 = s2.Length - eq
    match d1 + d2 with
      | x when x > n -> false
      | x when (n - x) % 2 = 0 -> true
      | _ -> false

let result1 = impl "aba" "aba" 7 //true
let result2 = impl "hackerhappy" "hackerrank" 9 //true
let result3 = impl "h" "hackerrank" 3 //false
let result4 = impl "abcd" "abcdert" 10 //fasle
let result5 = impl "hackerhappy" "hacsa" 9  //false
let result6 = impl "aaa" "a" 5 //true

//Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
//let rec equalLen (s1:char list) (s2:char list) = 
//  match (s1, s2) with
//    | ([], _) -> 0
//    | (_, []) -> 0
//    | (s1h::xs1, s2h::xs2) when s1h = s2h -> equalLen xs1 xs2 + 1
//    | (s1h::xs1, s2h::xs2) -> 0
//
//let impl (s1:string) (s2:string) n =
//  if (n >= s1.Length + s2.Length) then
//    true
//  else
//    let eq = equalLen (List.ofArray(s1.ToCharArray())) (List.ofArray(s2.ToCharArray()))
//    let d1 = s1.Length - eq
//    let d2 = s2.Length - eq
//    match d1 + d2 with
//      | x when x > n -> false
//      | x when (n - x) % 2 = 0 -> true
//      | _ -> false
//
//let s1 = Console.ReadLine()
//let s2 = Console.ReadLine()
//let n = Console.ReadLine() |> int
//
//let result = impl s1 s2 n
//if result then 
//  Console.WriteLine("Yes")
//else
//  Console.WriteLine("No")