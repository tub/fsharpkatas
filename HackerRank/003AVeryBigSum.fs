﻿module AVeryBigSum

let impl count elements =
  elements.ToString().Split()
  |> Array.take count
  |> Array.map int64
  |> Array.sum

let result = impl 5 "1000000001 1000000002 1000000003 1000000004 1000000005"

////Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
//[<EntryPoint>]
//let main argv = 
//  let count = Console.ReadLine() |> int
//  let elements = Console.ReadLine()
//    
//  let result =
//    elements.ToString().Split()
//    |> Array.take count
//    |> Array.map int64
//    |> Array.sum
//  printfn "%d" result
//  
//  0 // return an integer exit code