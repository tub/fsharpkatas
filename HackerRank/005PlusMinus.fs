﻿module PlusMinus
open System

let impl count elements =
  let nums =
    elements.ToString().Split()
    |> Array.take count
    |> Array.map int

  let pos = Array.filter (fun n -> n > 0) nums |> Array.length |> float
  let neg = Array.filter (fun n -> n < 0) nums |> Array.length |> float
  let zero = Array.filter (fun n -> n = 0) nums |> Array.length |> float
  (pos/(float count), neg/(float count), zero/(float count))

let (p,n,z) = impl 6 "-4 3 -9 0 4 1"

printfn "%.6f\n%.6f\n%.6f" p n z

////Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
//[<EntryPoint>]
//let main argv = 
//  let count = Console.ReadLine() |> int
//  let elements = Console.ReadLine()
//
//  let nums =
//    elements.ToString().Split()
//    |> Array.take count
//    |> Array.map int
//
//  let pos = Array.filter (fun n -> n > 0) nums |> Array.length |> float
//  let neg = Array.filter (fun n -> n < 0) nums |> Array.length |> float
//  let zero = Array.filter (fun n -> n = 0) nums |> Array.length |> float
//  printfn "%.6f\n%.6f\n%.6f" (pos/(float count)) (neg/(float count)) (zero/(float count))
//  
//  0 // return an integer exit code
