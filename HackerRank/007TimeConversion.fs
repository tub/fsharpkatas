﻿module TimeConversion
open System

let impl (str:string) =
  let time = str.Substring(0, str.Length - 2)
  printfn "time: %s" time
  let am = str.EndsWith("AM")
  let [|h; m; s|] = time.Split([|':'|], 3) |> Array.map int
  let converted_h =
    match (h, am) with
    | (12, true) -> 0
    | (12, false) -> 12
    | (x, false) -> x + 12
    | (x, true) -> x
  (converted_h, m, s)

let (h, m, s) = impl "07:05:45PM"
printf "%02d:%02d:%02d" h m s

//Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
//[<EntryPoint>]
//let main argv =
//  let str = Console.ReadLine()
//  let time = str.Substring(0, str.Length - 2)
//  let am = str.EndsWith("AM")
//  let [|h; m; s|] = time.Split([|':'|], 3) |> Array.map int
//  let converted_h =
//    match (h, am) with
//    | (12, true) -> 0
//    | (12, false) -> 12
//    | (x, false) -> x + 12
//    | (x, true) -> xh
//  printf "%02d:%02d:%02d" converted_h m s
//  
//  0 // return an integer exit code