﻿module Staircase
open System

let impl n =
  [1..n] |> List.map (fun i ->
    String.replicate (n - i) " " + String.replicate (i) "#")

let result = impl 6

printfn "%s" (String.Join("\n", result))

//Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
//[<EntryPoint>]
//let main argv = 
//  let n = Console.ReadLine() |> int
//  let strs = [1..n] |> List.map (fun i -> 
//    String.replicate (n - i) " " + String.replicate (i) "#")
//
//  printfn "%s" (String.Join("\n", strs))
//
//  0 // return an integer exit code