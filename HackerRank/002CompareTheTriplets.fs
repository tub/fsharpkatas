﻿module CompareTheTriplets

let impl aliceTriplets bobTriplets =
  let aliceVals = aliceTriplets.ToString().Split() |> Array.map int
  let bobVals = bobTriplets.ToString().Split() |> Array.map int
  Array.fold2 (fun (aliceScore, bobScore) a b -> 
    match (a, b) with
    | (x, y) when x > y -> (aliceScore + 1, bobScore) 
    | (x, y) when x < y -> (aliceScore, bobScore + 1) 
    | _ -> (aliceScore, bobScore) 
    ) (0, 0) aliceVals bobVals

let score = impl "5 6 7" "3 6 10"

printfn "%d %d" (fst score) (snd score)

//Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
//[<EntryPoint>]
//let main argv = 
//    let aliceVals = Console.ReadLine().Split() |> Array.map int
//    let bobVals = Console.ReadLine().Split() |> Array.map int
//    let score = 
//        Array.fold2 (fun (aScore, bScore) a b -> 
//          match (a, b) with
//          | (x, y) when x > y -> (aScore + 1, bScore) 
//          | (x, y) when x < y -> (aScore, bScore + 1) 
//          | _ -> (aScore, bScore) 
//        ) (0, 0) aliceVals bobVals
//        
//    printfn "%d %d" (fst score) (snd score)
//  