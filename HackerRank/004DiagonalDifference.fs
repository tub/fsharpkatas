﻿module DiagonalDifference
open System

let impl count (rows:string []) =
  let matrix = Array.init count (fun num -> rows.[num].ToString().Split() |> Array.map int)
  let diag1 = [0..(count-1)] |> List.map (fun x -> matrix.[x].[x]) |> List.sum
  let diag2 = [0..(count-1)] |> List.map (fun x -> matrix.[x].[count - 1 - x]) |> List.sum
  Math.Abs (diag1 - diag2)

let result = impl 3 [| "11 2 4"; "4 5 6"; "10 8 -12" |]

//Enter your code here. Read input from STDIN. Print output to STDOUT
//open System
//
//[<EntryPoint>]
//let main argv = 
//  let count = Console.ReadLine() |> int
//  let matrix = Array.init count (fun _ -> Console.ReadLine().Split() |> Array.map int)
//  let diag1 = [0..(count-1)] |> List.map (fun x -> matrix.[x].[x]) |> List.sum
//  let diag2 = [0..(count-1)] |> List.map (fun x -> matrix.[x].[count - 1 - x]) |> List.sum
//  let result = Math.Abs (diag1 - diag2)
//  
//  printfn "%d" result
//  
//  0 // return an integer exit code